$(document).ready(loadEvents);

function loadEvents(){//eventos principales 
    validateFormLogin();
}

function validateFormLogin(){
    $('#form-login').validate({
        rules:{
            user: "required",
            password: "required"
        }
    });
    $('.sub-button').click(function(){
        if($('#form-login').valid()){
    		$('#form-login').submit();
    	}
    })
}