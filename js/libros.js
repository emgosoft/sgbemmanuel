$(document).ready(loadEvents);

function loadEvents(){//eventos principales
    var action =  $('#action').val();
    console.log(action);
    if(action == "index")
        getJsonDataTable();
    
    $("li#principal-page-index").removeClass('active-li');
    validateFormCreateBook();
    validateFormUpdateBook();
}
function validateFormUpdateBook(){
    $('#form-update-book').validate({
        rules:{
            titulo: "required",
            autor: "required",
            editorial: "required",
            isbn: "required",
            anio_edicion: "required",
            genero: "required",
            disponible: "required",
            existencia: "required"
        }
    });
    $('#submit-form-update-book').click(function(){
        var formValid =  $('#form-update-book').valid();
        if(formValid)
            postUpdateBook();
    });
}
function postUpdateBook(){//enviar informacion del formulario "crear libro"
    $.ajax({
        type: "POST",
        url: "../../DBphp/libros.php",
        data: $('#form-update-book').serialize(),
        success: function(response){
            console.log(response);
            if(response != "success"){
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al modificar el libro",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                })  
            }
            else{
                swal({
                    title: 'Libro modificado',
                    text: "¡Se ha modificado el libro con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'libros.php';
                    }
                })
            }
        }
    }) 
}
function validateFormCreateBook(){//validar formulario crear libro
    $('#form-create-book').validate({
        rules:{
            titulo: "required",
            autor: "required",
            editorial: "required",
            isbn: "required",
            anio_edicion: "required",
            genero: "required",
            disponible: "required",
            existencia: "required"
        }
    });
    $('#btn-create-book').click(function(){
        var formValid =  $('#form-create-book').valid();
        if(formValid)
            postCreateBook();
    });
}
function postCreateBook(){//enviar informacion del formulario "crear libro"
    $.ajax({
        type: "POST",
        url: "../../DBphp/libros.php",
        data: $('#form-create-book').serialize(),
        success: function(response){
            if(response != "success"){
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al agregar el libro",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                })  
            }
            else{
                swal({
                    title: 'Libro agregado',
                    text: "¡Se ha agregado el libro con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'libros.php';
                    }
                })
            }
        }
    }) 
}

function visualizeDataBook(idBook){//visualizar informacion del libro en un modal
    $.ajax({
        data : {"idBook": idBook, action: "view"},
        url : "../../DBphp/libros.php",
        method : "POST",
        success: function(response){
            var responseParse = jQuery.parseJSON(response)
            $.each(responseParse, function(index, value){
                var availableBook = value.disponible;
                if(availableBook == 1)
                    availableBook = "Hay disponibles";
                else
                    availableBook = "No hay disponibles";
                $('#tituloLibro').attr('value', value.titulo);
                $('#autorLibro').attr('value', value.autor);
                $('#editorialLibro').attr('value', value.editorial);
                $('#isbnLibro').attr('value', value.isbn);
                $('#anioEdicionLibro').attr('value', value.anio_edicion);
                $('#generoLibro').attr('value', value.genero);
                $('#disponibleLibro').attr('value', availableBook);
                $('#existenciaLibro').attr('value', value.existencia);
            });
            $('#viewDataBook').modal();
        }
    });
}

function getJsonDataTable(){
    $.ajax({
         url : "../../DBphp/libros.php",
         method : "POST",
         data : {action: "index"},
         success: function(response){
             var responseParse = jQuery.parseJSON(response);
             initializeTable(responseParse);
         }
     });   
}

function initializeTable(data){//filtro de informacion en tabla de libros
    var typeUser = $('#tipo').val();
    var dataTableOptions ={
        data: data,
        "columns": [
            {data : "titulo"},
            {data : "autor"},
            {data : "editorial"},
            {data : "genero"},
            {data : "existencia"},
            {data: "Options", render: function (data, type, row) {
                if(typeUser != 'Bibliotecario'){
                    return '<button type="button" onclick="visualizeDataBook(\'' + row.id + '\');" class="btn btn-secondary btn-visualize-data">'+
                                '<i class="fas fa-eye icon-btn-options"></i>'+
                            '</button>';
                }else{
                    return '<button type="button" onclick="visualizeDataBook(\'' + row.id + '\');" class="btn btn-secondary btn-visualize-data">'+
                                '<i class="fas fa-eye icon-btn-options"></i>'+
                            '</button>'+
                            '<a type="button" class="btn btn-secondary btn-edit-data" href="bookEdit.php?idBook=' + row.id + '">'+
                                '<i class="fas fa-edit icon-btn-options "></i>'+
                            '</a>';
                }
              }
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Búsqueda",
            "paginate": {
                "first":      "Inicio",
                "last":       "Fin",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    }
    $('#book-table').DataTable(dataTableOptions);
}