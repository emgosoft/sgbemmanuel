$(document).ready(loadEvents);

function loadEvents(){//eventos principales
    var action = $('#action').val()
    console.log(action)
    if(action == "index"){
        getJsonDataTable();
    }
    $("li#principal-page-index").removeClass('active-li');
    validateFormCreateMaterial();
    validateFormUpdateMaterial();
}
function validateFormUpdateMaterial(){
    $('#form-update-material').validate({
        rules:{
            tipo: "required",
            marca: "required",
            modelo: "required",
            estado: "required"
        }
    });
    $('#submit-form-update-material').click(function(){
        var formValid =  $('#form-update-material').valid();
        if(formValid)
            postUpdateMaterial();
    });
}
function postUpdateMaterial(){
    $.ajax({
        type: "POST",
        url: "../../DBphp/material.php",
        data: $('#form-update-material').serialize(),
        success: function(response){
            console.log(response);
            if(response != "success"){
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al modificar el material",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                })  
            }
            else{
                swal({
                    title: 'Material modificado',
                    text: "¡Se ha modificado el material con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'material.php';
                    }
                })
            }
        }
    }) 
}
function validateFormCreateMaterial(){
    $('#form-create-material').validate({
        rules:{
            tipo: "required",
            marca: "required",
            modelo: "required",
            estado: "required"
        }
    });
    $('#btn-create-material').click(function(){
        var formValid =  $('#form-create-material').valid();
        if(formValid)
            postCreateMaterial();
    });
}
function postCreateMaterial(){
    $.ajax({
        type: "POST",
        url: "../../DBphp/material.php",
        data: $('#form-create-material').serialize(),
        success: function(response){
            console.log(response);
            if(response != "success"){
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al agregar el material",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                })  
            }
            else{
                swal({
                    title: 'Material agregado',
                    text: "¡Se ha agregado el material con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'material.php';
                    }
                })
            }
        }
    }) 
}
function visualizeDataMaterial(idMaterial){
    $.ajax({
        data : {"idMaterial": idMaterial, action:"view"},
        url : "../../DBphp/material.php",
        method : "POST",
        success: function(response){
            $('#viewDataMaterial').modal();
            var responseParse = jQuery.parseJSON(response)
            $.each(responseParse, function(index, value){
                $('#typeMaterial').attr('value', value.tipo);
                $('#brandMaterial').attr('value', value.marca);
                $('#modelMaterial').attr('value', value.modelo);
                $('#statusMaterial').attr('value', value.estado);
            });
        }
    });
}
function getJsonDataTable(){
    $.ajax({
         url : "../../DBphp/material.php",
         method : "POST",
         data : {action: "index"},
         success: function(response){
             var responseParse = jQuery.parseJSON(response);
             initializeTable(responseParse);
         }
     });   
}
function initializeTable(data){//filtro de informacion en tabla de usuarios
    var typeUser = $('#tipo').val();
    var dataTableOptions ={
        data: data,
        "columns": [
            {data : "tipo"},
            {data : "marca"},
            {data : "modelo"},
            {data: "Options", render: function (data, type, row) {
                if(typeUser != 'Bibliotecario'){
                    return '<div class="btn-group" role="group" aria-label="Basic example">'+
                                '<button type="button" onclick="visualizeDataMaterial(\'' + row.id + '\');" class="btn btn-secondary btn-visualize-data">'+
                                    '<i class="fas fa-eye icon-btn-options"></i>'+
                               '</button>'+
                            '</div>';
                }else{
                    return '<div class="btn-group" role="group" aria-label="Basic example">'+
                                '<button type="button" onclick="visualizeDataMaterial(\'' + row.id + '\');" class="btn btn-secondary btn-visualize-data">'+
                                    '<i class="fas fa-eye icon-btn-options"></i>'+
                               '</button>'+
                                '<a type="button" class="btn btn-secondary btn-edit-data" href="materialEdit.php?idMaterial=' + row.id + '">'+
                                    '<i class="fas fa-edit icon-btn-options "></i>'+
                                '</a>'+
                            '</div>';
                }
              }
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Búsqueda",
            "paginate": {
                "first":      "Inicio",
                "last":       "Fin",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    }
    $('#material-table').DataTable(dataTableOptions);
}