$(document).ready(loadEvents);

function loadEvents(){//eventos principales
    var action = $('#action').val();
    console.log(action)
    if(action == "index")
        getJsonDataTable();
    else if(action == "update")
        validateFormUpdateUser();
    else if(action == "create")
        validateFormCreateUser();
    
    $("li#principal-page-index").removeClass('active-li');
}
function validateFormUpdateUser(){
    $('#form-update-user').validate({
        rules:{
            nombre: "required",
            numero_celular: "required",
            contraseña: "required",
            genero: "required",
            correo: "required",
            estatus: "required",
            tipo: "required",
            area: "required"
        }
    });
    $('#submit-form-update-user').click(function(){
        var formValid =  $('#form-update-user').valid();
        if(formValid)
            postUpdateUser();
    });
}
function postUpdateUser(){//enviar informacion del formulario "crear usuario"
    $.ajax({
        type: "POST",
        url: "../../DBphp/usuarios.php",
        data: $('#form-update-user').serialize(),
        success: function(response){
            console.log(response);
            if(response != "success"){
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al modificar el usuario",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                })  
            }
            else{
                swal({
                    title: 'Usuario modificado',
                    text: "¡Se ha modificado el usuario con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'usuarios.php';
                    }
                })
            }
        }
    })
}
function validateFormCreateUser(){//validar formulario crear usurio
    $('#form-create-user').validate({
        rules:{
            name: "required",
            numero_celular: "required",
            contraseña: "required",
            genero: "required",
            correo: "required",
            estatus: "required",
            tipo: "required",
            area: "required"
        }
    });
    $('#btn-create-user').click(function(){
        var formValid =  $('#form-create-user').valid();
        if(formValid)
            postCreateUser();
    });
}
function postCreateUser(){//enviar informacion del formulario "crear usuario"
    var nombre = $("#nombre").val();
    var numeroCelular = $("#numero_celular").val();
    var contrasenia = $("#contraseña").val();
    var genero = $("#genero").children("option:selected").attr('value');
    var correo = $("#correo").val();
    var estatus = $("#estatus").children("option:selected").attr('value');
    var tipo = $('#tipo-usuario').children("option:selected").attr('value');
    var area = $("#area").val();
    $.ajax({
        type: "POST",
        url: "../../DBphp/usuarios.php",
        data: {
            nombre: nombre,
            numero_celular: numeroCelular,
            contrasenia: contrasenia,
            genero: genero,
            correo: correo,
            estatus: estatus,
            tipo: tipo,
            area: area,
            action: "create"},
        success: function(response){
            if(response == "success"){
                swal({
                    title: 'Usuario agregado',
                    text: "¡Se ha agregado el usuario con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'usuarios.php';
                    }
                });
            }
            else{
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al agregar el usuario",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }); 
            }
        }
    });
}
function visualizeDataUser(idUser){//visualizar informacion del usuario en un modal
    $.ajax({
        data : {"idUser": idUser, action: "view"},
        url : "../../DBphp/usuarios.php",
        method : "POST",
        success: function(response){
            $('#viewDataUser').modal();
            var responseParse = jQuery.parseJSON(response)
            $.each(responseParse, function(index, value){
                var statusUser = value.estatus;
                var genero = value.genero;
                if(statusUser == 1)
                    statusUser = "Activo";
                else
                    statusUser = "Inactivo";
                
                if(genero == "M")
                    genero = "Masculino";
                else if(genero == "F")
                    genero = "Femenino";
                
                $('#nameUser').attr('value', value.nombre);
                $('#telephoneUser').attr('value', value.numero_celular);
                $('#passwordUser').attr('value', value.password);
                $('#genderUser').attr('value', genero);
                $('#emailUser').attr('value', value.correo);
                $('#borrowedBooksUser').attr('value', value.libros_prestados);
                $('#statusUser').attr('value', statusUser);
                $('#typeUser').attr('value', value.tipo);
                $('#areaUser').attr('value', value.area);
            });
        }
    });
}
function getJsonDataTable(){
   $.ajax({
        url : "../../DBphp/usuarios.php",
        method : "POST",
        data : {action: "index"},
        success: function(response){
            var responseParse = jQuery.parseJSON(response);
            initializeTable(responseParse);
        }
    });   
}
function initializeTable(data){//filtro de informacion en tabla de usuarios
    var dataTableOptions ={
        data: data,
        "columns": [
            {data : "nombre"},
            {data : "numero_celular"},
            {data : "correo"},
            {data : "area"},
            {data : "Options", render: function (data, type, row) {
                return '<div class="btn-group" role="group" aria-label="Basic example">'+
                        '<button type="button" onclick="visualizeDataUser(\'' + row.id + '\');" class="btn btn-secondary btn-visualize-data">'+
                            '<i class="fas fa-eye icon-btn-options"></i>'+
                        '</button>'+
                        '<a type="button" class="btn btn-secondary btn-edit-data" href="userEdit.php?idUser=' + row.id + '">'+
                            '<i class="fas fa-edit icon-btn-options "></i>'+
                        '</a></div>';
              }
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Búsqueda",
            "paginate": {
                "first":      "Inicio",
                "last":       "Fin",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    }
    $('#users-table').DataTable(dataTableOptions);
}