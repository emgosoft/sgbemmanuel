$(document).ready(loadEvents);


const maxLoanBooks = 5;
var availableBooks = 0;


function loadEvents(){//eventos principales
    var action = $("#view").val();
    console.log(action);
    switch(action){
        case "index":
            getJsonDataTable();
            $('#content-penality').hide();
        break;
        case "create":
            $('#date').datepicker({
                dateFormat: "yy-mm-dd"
            });
            fillIdRequestingUser();
            fillFolio();
            borrowedBooks();
            fillBooksRepeater();
            createLoanBook();
        break;
        case "loanBook":
            postReturnLoanBook();
            postDataReturnLoanBooks();
            $('#content-penality').hide();
            $('#required-penality').change(checkPenality);
        break;
        default: alert("Error"); break;
    }

    $("li#principal-page-index").removeClass('active-li');
}

function fillIdRequestingUser(){//filtro de usuarios tipo estudiante y docente
    $.ajax({
        type: "POST",
        url: "../../DBphp/prestamoLibros.php",
        data: {action: "fillSelect"},
        success: function(response){
           var data = JSON.parse(response);
           console.log(data);
           var content = "";
           for(var i = 0; i < data.length; i++){
               content += '<option value="'+ data[i].id +'">'+ data[i].nombre +'</option>';
           }
           $('#idRequestingUser').append(content);
        }
    }); 
}

function fillFolio(){//filtro del nuevo folio a crear
    $.ajax({
        type: "POST",
        url: "../../DBphp/prestamoLibros.php",
        data: {action: "fillFolio"},
        success: function(data){
           $('#folio').attr('value', data);
        }
    }); 
}

function borrowedBooks(){//obtener cantidad de libros prestados por usuarios
    $('#idRequestingUser').on('change', function(){
        var idUserBorrowedBooks = $(this).children("option:selected").attr('value');
        $.ajax({
            type: "POST",
            url: "../../DBphp/prestamoLibros.php",
            data: {action: "borrowedBooks", idUserBorrowedBooks: idUserBorrowedBooks},
            success: function(data){
                var totalBorrowedBooks = data;
                const totalAllowableLoans = 5;
                availableBooks = maxLoanBooks - totalBorrowedBooks;
               if(totalBorrowedBooks < totalAllowableLoans){
                   $('#total-borrowed-books-title').html("El usuario tiene un total de: "+ totalBorrowedBooks +" préstamos");
                   $('#total-borrowed-books-title').css({"color" : "green"});
               }else if(totalBorrowedBooks == totalAllowableLoans){
                    $('#total-borrowed-books-title').html("El usuario tiene un total de: "+ totalBorrowedBooks +" préstamos");
                    $('#total-borrowed-books-title').css({"color" : "red"});
               }

            }
        });
    });
}

function fillBooksRepeater(){//filtrar informacion al repeater
    $('.add-registry').on('click', function(){
        const table = $('#loan-books-table');
        if( table.find('tr').length > availableBooks){
            return;
        }
        $.ajax({
            type: "POST",
            url: "../../DBphp/prestamoLibros.php",
            data: {action: "fillBooksRepeater"},
            success: function(data){
                var folio = $('#folio').val();
                var html = '';
                    html += '<tr>';
                    html += '<td><input type="text" name="folio[]" value="'+folio+'" class="form-control folio" style="text-align:center;" disabled/></td>';
                    html += '<td><select name="book[]" class="form-control book"><option value="">Selecciona opcion</option>'+data+'</select><span id="existencia"></span></td>';
                    html += '<td><select name="extension[]" class="form-control extension"><option value="">Selecciona opcion</option><option value="0">No</option><option value="1">Sí</option></select></td>';
                    html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm removeRepeater"><span style="font-size: 20px;font-weight: 900; margin: 5px;">-</span></button></td></tr>';
                $('#loan-books-table').append(html);
                $('.removeRepeater').on('click', function(){//crear evento remove
                    $(this).closest('tr').remove();
                });
                $('.book').on('change', function(){
                    var existencia = $(this).children("option:selected").attr('existencia');
                    if(existencia == 0){
                        $(this).closest('td').find('span').css({"color":"red", "font-size":"12px"});
                        $(this).closest('td').find('span').text("Existencia: " + existencia);
                    }else{
                        $(this).closest('td').find('span').css({"color":"blue", "font-size":"12px"});
                        $(this).closest('td').find('span').text("Existencia: " + existencia);   
                    }
                });
            }
        });
    });
}

function createLoanBook(){
    $('#btn-create-loan-book').on('click', function(){
        postCreateLoanBook();
    });
}


function postCreateLoanBook(){
    var folio = $('#folio').val();
    var book = [];
    var extension = [];
    var date = $("#date").val();
    var idRequestingUser =  $("#idRequestingUser").children("option:selected").attr('value');
    var idUserLoan = $('#idUserLoan').val();
    $('.book').each(function(){
        book.push($(this).children("option:selected").attr('value'));
    });
    $('.extension').each(function(){
        extension.push($(this).children("option:selected").attr('value'));
    });
    $.ajax({
        url : "../../DBphp/prestamoLibros.php",
        method : "POST",
        data : {folio: folio, book:book, extension:extension, action: "create", date: date, idRequestingUser: idRequestingUser, idUserLoan: idUserLoan},
        success: function(response){
            if(response == "success"){
                swal({
                    title: 'Préstamo Bibliografico',
                    text: "¡Se ha agregado el prestamo con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'prestamoLibros.php';
                    }
                }); 
            }
            else{
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al agregar el prestamo",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }); 
            }
        }
    });
}

function getJsonDataTable(){
    var idUser = $("#id").val();
    var typeUser = $("#tipo").val();
    var action = $("#view").val();
   $.ajax({
        url : "../../DBphp/prestamoLibros.php",
        method : "POST",
        data : {idUser: idUser, typeUser: typeUser, action: action},
        success: function(response){
            var responseParse = jQuery.parseJSON(response);
            initializeTable(responseParse, typeUser);
        }
    });   
}

function initializeTable(data, typeUser){//filtro de informacion en tabla de prestamos
    var dataTableOptions = {
        data: data,
        "columns": [
            {data : "folio"},
            {data : "fecha"},
            {data : "usuario"},
            {data : "libros"},
            {data: "Options", render: function (data, type, row) {
                if(typeUser != 'Bibliotecario'){
                    return '<button type="button" onclick="visualizeLoanBook(\'' + row.folio + '\');" class="btn btn-secondary btn-visualize-data">'+
                            '<i class="fas fa-eye icon-btn-options"></i>'+
                        '</button>';
                }else{
                    return '<button type="button" onclick="visualizeLoanBook(\'' + row.folio + '\');" class="btn btn-secondary btn-visualize-data">'+
                                '<i class="fas fa-eye icon-btn-options"></i>'+
                            '</button>'+
                            '<a type="button" class="btn btn-secondary btn-primary-data btn-return-loan-book" href="returnLoanBook.php?folio=' + row.folio + '" style="margin-left:3%;">Devolución</a>';
                }
              }
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Búsqueda",
            "paginate": {
                "first":      "Inicio",
                "last":       "Fin",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    }
    $('#loan-books-table').DataTable(dataTableOptions);
}

function visualizeLoanBook(folio){//visualizar detalles del prestamo
    $.ajax({
        url : "../../DBphp/prestamoLibros.php",
        method : "POST",
        data : {action: "view", folio: folio},
        success: function(response){
            $('#viewDataLoanBook').modal();
            var responseParse = JSON.parse(response);
            var data = responseParse.data;
            var content = "";
            $.each(responseParse, function(index, value){
                $('#folio').attr('value', value.folio);
                $('#date').val(value.fecha);
                $('#user').val(value.usuario);
                $('#employee').val(value.empleado);
                $.each(value.libros, function(index, value){
                    var numItem = 0;
                    numItem = (parseInt(index) + parseInt(1));

                    if(value.extension == 0)
                        extension = "Sin extensión";
                    else
                        extension = "Con extensión";
                    
                    if(value.fecha_devolucion == null){
                        fechaDevolucion = "No se ha entregado"
                    }
                    else{
                        fechaDevolucion = value.fecha_devolucion;
                    }
                    content += '<tr>'+
                            '<th scope="row">'+ numItem +'</th>'+
                            '<td class="text-center">'+value.titulo+'</td>'+
                            '<td class="text-center">'+value.genero+'</td>'+
                            '<td class="text-center">'+value.isbn+'</td>'+
                            '<td class="text-center">'+extension+'</td>'+
                            '<td class="text-center">'+fechaDevolucion+'</td>'+
                        '</tr>';
                    $('#data-loan-books-table').find('tbody').html(content);
                });
            });
            if(data.penalizacion != null){
                $('#content-penality').show();
                $('#folioPenalizacion').val(data.penalizacion.folio);
                $('#motivo').val(data.penalizacion.motivo);
                $('#totalPenalizacion').val(data.penalizacion.total);
            }else{
                $('#content-penality').hide();
            }
        }
    });
}
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}
function postReturnLoanBook(){
    const folio = getUrlParameter('folio');
   $.ajax({
        url : "../../DBphp/prestamoLibros.php",
        method : "POST",
        data : {folio: folio, action: "loanBooks"},
        success: function(response){
            var responseParse = jQuery.parseJSON(response);
            console.log(responseParse);
            const data = responseParse.data;
            var content;
            $('#folio').val(data.folio);
            $('#date').val(data.fecha)
            $('#user').val(data.nombre)
            $('#employee').val(data.empleado)
            $('#user-id').val(data.usuario);
            $.each(data.libros, function(index, value){
                var numItem = 0;
                numItem = (parseInt(index) + parseInt(1));

                if(value.extension == 0)
                    extension = "Sin extensión";
                else
                    extension = "Con extensión";
                
                if(value.fecha_devolucion == null){
                    fechaDevolucion = "No se ha entregado";
                    
                } else {
                    fechaDevolucion = value.fecha_devolucion;
                }

                content += '<tr>'+
                        '<th scope="row">'+ numItem +'</th>'+
                        '<td class="text-center"><span></span><input type="hidden" value="'+value.id+'" class="id-book" />'+value.titulo+'</td>'+
                        '<td class="text-center">'+value.isbn+'</td>'+
                        '<td class="text-center">'+extension+'</td>'+
                        '<td class="text-center">'+
                            '<label class="switch">'+
                                '<input  class="devolucion devolucion-check" type="checkbox"'; 
                    if (value.fecha_devolucion != null) {
                        content += ' checked disabled';
                    }
                    content += '/><span class="slider round"></span>'+
                            '</label>'+
                        '</td>'+
                        '<td class="text-center"><span class="return-date">'+fechaDevolucion+'</span></td>'+
                    '</tr>';
                $('#data-loan-books-table').find('tbody').html(content);
            });
            if(data.penalizacion) {
                fillPenality(data.penalizacion);
            }
            getReturnDate();
            checkTotalLoans();
        }
    }); 
}
function fillPenality(penality){
    $('#required-penality').attr('checked', true);
    $('#required-penality').attr('disabled', true);
    $('#content-penality').show();
    $('#folioPenalizacion').val(penality.folio);
    $('#motivo').val(penality.motivo);
    $('#totalPenalizacion').val(penality.total);
}
function checkTotalLoans(){
    var contador = 0;
    var totalChecks =  $('.devolucion-check').length;
    $('.devolucion-check').each(function(index, value){
        if($(this).is(':checked')){
            contador = parseInt(contador) + 1;   
        }
        if(contador == totalChecks){
            $('#required-penality').attr('disabled', true);
            $('.m-body-footer-btns').html('<a href="prestamoLibros.php" class="btn btn-outline-primary btn-lg btn-block">Regresar</a>')
        }
    });
}

function getReturnDate(){
    $('.devolucion-check').on('change', function(){
        if( $(this).is(':checked') ){
            var date = getDate();
            $(this).closest('tr').find('.return-date').text(date);
        } else {
            $(this).closest('tr').find('.return-date').text("No se ha entregado");
        }
    }); 
}

function getDate(){
    let date = new Date();
    let dd = date.getDate();
    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if(dd < 10){
        dd = '0' + dd;
    }
    if(mm < 10){
        mm = '0' + mm;
    }
    date = yyyy + '-' + mm + '-' + dd;
    return date;
}

function getReturnValues(){
    let books = [];
    const table = $('#data-loan-books-table');
    table.find('tbody').find('tr').each(function(){
        let returned = $(this).find('.devolucion-check').prop('checked');
        let disabled = $(this).find('.devolucion-check').prop('disabled');
        if(returned && !disabled){
            let bookId = $(this).find('.id-book').val();
            let returnDate = getDate();
            books.push({'id': bookId, 'returned_date': returnDate});
        }         
    });
    books = JSON.stringify(books);
    return books;
}
function postDataReturnLoanBooks(){
    $('#submit-form-update-book').on('click', function(){
        let valuesRegistry = getReturnValues();
        let folio = $('#folio').val();
        let userId = $('#user-id').val();
        let folioPenality = $('#folioPenalizacion').val();
        let reasonPenality = $('#motivo').val();
        let totalPenality = $('#totalPenalizacion').val();
        if($('#required-penality').is(':checked')){
            var flagNewFolio = "insert";
            if($('#required-penality').is(':disabled')){
                flagNewFolio = "update";
            }
            data = {'action': "update", 'folio': folio, 'books': valuesRegistry, 'idRequestingUser': userId, 'folioPenality': folioPenality, 'reasonPenality': reasonPenality, 'totalPenality': totalPenality, 'newRegistry': flagNewFolio};
        }else{
            data = {'action': "update", 'folio': folio, 'books': valuesRegistry, 'idRequestingUser': userId};
        }
        $.ajax({
            url : "../../DBphp/prestamoLibros.php",
            method : "POST",
            data : data,
            success: function(response){
                sweetAlerts(response);
            }
        });
    });
}

function sweetAlerts(response){
    if(response == "success"){
        swal({
            title: 'Devolución Bibliografico',
            text: "¡Se ha agregado la devolución con exito!",
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
            }).then((result) => {
            if (result.value) {
                window.location.href = 'prestamoLibros.php';
            }
        }); 
    }
    else{
        swal({
            title: 'Error',
            text: "Ha ocurrido un error al agregar la devolución",
            type: 'error',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
        }); 
    }
}

function checkPenality(){
    if ($(this).is(':checked')) {
        $('#content-penality').show();
        folioPenality();
    } else {
        $('#content-penality').hide();
    }
}

function folioPenality(){
    $.ajax({
        url : "../../DBphp/prestamoLibros.php",
        method : "POST",
        data : {action: "receiveFolioPenality"},
        success: function(response){
            $('#folioPenalizacion').val(response);
        }
    });
}

