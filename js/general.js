
function loadEvents(){//eventos principales del programa
    $('.opt-sesion').hide();
    stopSesion();
    changeActiveUrl();
}

function stopSesion(){//mostrar opcion de cerrar sesion
    $('#user-options').click(function(){
        $('.opt-sesion').toggle();
    }); 
}

function changeActiveUrl(){//cambiar activo o inactivo li del sidebar
    $('ul a.extern-page').filter(function(){return this.href==location.href}).parent().addClass('active-li').siblings().removeClass('active-li');
	$('ul a.extern-page').click(function(){
		$(this).parent().addClass('active-li').siblings().removeClass('active-li')	
	});
}

$(document).ready(loadEvents);
