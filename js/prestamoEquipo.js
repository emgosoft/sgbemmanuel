$(document).ready(loadEvents);

function loadEvents(){//eventos principales
    var action = $("#view").val();
    switch(action){
        case "index":
            getJsonDataTable();
            $('#content-penality').hide();
        break;
        case "create":
            $('#date').datepicker({
                dateFormat: "yy-mm-dd"
            });
            fillIdRequestingUser();
            fillFolio();
            fillMaterialRepeater();
            createLoanEquipment();
        break;
        case "loanMaterial":
            postReturnLoanMaterial();
            postDataReturnLoanMaterials();
            $('#content-penality').hide();
            $('#required-penality').change(checkPenality);
        break;
        default: alert("Error"); break;
    }

    $("li#principal-page-index").removeClass('active-li');
}

function fillIdRequestingUser(){//filtro de usuarios tipo docente
    $.ajax({
        type: "POST",
        url: "../../DBphp/prestamoEquipo.php",
        data: {action: "fillSelect"},
        success: function(response){
           var data = JSON.parse(response);
           var content = "";
           for(var i = 0; i < data.length; i++){
               content += '<option value="'+ data[i].id +'">'+ data[i].nombre +'</option>';
           }
           $('#idRequestingUser').append(content);
        }
    }); 
}

function fillFolio(){//filtro del nuevo folio a crear
    $.ajax({
        type: "POST",
        url: "../../DBphp/prestamoEquipo.php",
        data: {action: "fillFolio"},
        success: function(data){
           $('#folio').attr('value', data);
        }
    }); 
}

function fillMaterialRepeater(){//filtrar informacion al repeater
    $('.add-registry').on('click', function(){
        $.ajax({
            type: "POST",
            url: "../../DBphp/prestamoEquipo.php",
            data: {action: "fillMaterialRepeater"},
            success: function(data){
                var folio = $('#folio').val();
                var html = '';
                    html += '<tr>';
                    html += '<td><input type="text" name="folio[]" value="'+folio+'" class="form-control folio" style="text-align:center;" disabled/></td>';
                    html += '<td><select name="material[]" class="form-control material"><option value="">Selecciona opcion</option>'+data+'</select><span id="existencia"></span></td>';
                    html += '<td><input type="text"  class="form-control typeMaterial" disabled/></td>';
                    html += '<td><input type="text"  class="form-control brandMaterial" disabled/></td>';
                    html += '<td><textarea type="text"  class="form-control statusMaterial" col="3" row="10" disabled></textarea></td>';
                    html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm removeRepeater"><span style="font-size: 20px;font-weight: 900; margin: 5px;">-</span></button></td></tr>';
                $('#loan-equipment-table').append(html);
                $('.removeRepeater').on('click', function(){//crear evento remove
                    $(this).closest('tr').remove();
                });
                $('.material').on('change', function(){
                    var tipo = $(this).children("option:selected").attr('tipo');
                    var estado = $(this).children("option:selected").attr('estado');
                    var marca = $(this).children("option:selected").attr('marca');

                    $(this).closest('tr').find('.typeMaterial').attr('value',tipo);
                    $(this).closest('tr').find('.statusMaterial').val(estado);
                    $(this).closest('tr').find('.brandMaterial').attr('value',marca);
                });
            }
        });
    });
}

function createLoanEquipment(){
    $('#btn-create-loan-equipment').on('click', function(){
        postCreateLoanEquipment();
    });
}


function postCreateLoanEquipment(){
    var folio = $('#folio').val();
    var material = [];
    var date = $("#date").val();
    var idRequestingUser =  $("#idRequestingUser").children("option:selected").attr('value');
    var idUserLoan = $('#idUserLoan').val();
    $('.material').each(function(){
        material.push($(this).children("option:selected").attr('value'));
    });
    $.ajax({
        url : "../../DBphp/prestamoEquipo.php",
        method : "POST",
        data : {folio: folio, material: material, action: "create", date: date, idRequestingUser: idRequestingUser, idUserLoan: idUserLoan},
        success: function(response){
            if(response == "success"){
                swal({
                    title: 'Préstamo Equipo de Computo',
                    text: "¡Se ha agregado el prestamo con exito!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                    }).then((result) => {
                    if (result.value) {
                        window.location.href = 'prestamoEquipo.php';
                    }
                }); 
            }
            else{
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error al agregar el prestamo",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }); 
            }
        }
    });
}

function getJsonDataTable(){
    var idUser = $("#id").val();
    var typeUser = $("#tipo").val();
    var action = $("#view").val();
   $.ajax({
        url : "../../DBphp/prestamoEquipo.php",
        method : "POST",
        data : {idUser: idUser, typeUser: typeUser, action: action},
        success: function(response){
            var responseParse = jQuery.parseJSON(response);
            initializeTable(responseParse, typeUser);
        }
    });   
}

function initializeTable(data, typeUser){//filtro de informacion en tabla de prestamos
    var dataTableOptions = {
        data: data,
        "columns": [
            {data : "folio"},
            {data : "fecha"},
            {data : "usuario"},
            {data : "material"},
            {data: "Options", render: function (data, type, row) {
                if(typeUser != 'Bibliotecario'){
                    return '<button type="button" onclick="visualizeLoanEquipment(\'' + row.folio + '\');" class="btn btn-secondary btn-visualize-data">'+
                            '<i class="fas fa-eye icon-btn-options"></i>'+
                        '</button>';
                }else{
                    return '<button type="button" onclick="visualizeLoanEquipment(\'' + row.folio + '\');" class="btn btn-secondary btn-visualize-data">'+
                                '<i class="fas fa-eye icon-btn-options"></i>'+
                            '</button>'+
                            '<a type="button" class="btn btn-secondary btn-primary-data btn-return-loan-book" href="returnLoanMaterial.php?folio=' + row.folio + '" style="margin-left:3%;">Devolución</a>';
                }
              }
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Búsqueda",
            "paginate": {
                "first":      "Inicio",
                "last":       "Fin",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    }
    $('#loan-equipment-table').DataTable(dataTableOptions);
}

function visualizeLoanEquipment(folio){//visualizar detalles del prestamo
    $.ajax({
        url : "../../DBphp/prestamoEquipo.php",
        method : "POST",
        data : {action: "view", folio: folio},
        success: function(response){
            $('#viewDataLoanEquipment').modal();
            var responseParse = JSON.parse(response);
            var data = responseParse.data;
            var content = "";
            $.each(responseParse, function(index, value){
                $('#folio').attr('value', value.folio);
                $('#date').val(value.fecha);
                $('#user').val(value.usuario);
                $('#employee').val(value.empleado);
                $.each(value.materiales, function(index, value){
                    var numItem = 0;
                    numItem = (parseInt(index) + parseInt(1));

                    if(value.fecha_devolucion == null){
                        fechaDevolucion = "No se ha entregado"
                    } else {
                        fechaDevolucion = value.fecha_devolucion;
                    }

                    content += '<tr>'+
                            '<th scope="row">'+ numItem +'</th>'+
                            '<td class="text-center">'+value.modelo+'</td>'+
                            '<td class="text-center">'+value.tipo+'</td>'+
                            '<td class="text-center">'+value.marca+'</td>'+
                            '<td class="text-center">'+value.estado+'</td>'+
                            '<td class="text-center">'+fechaDevolucion+'</td>'+
                        '</tr>';
                    $('#data-loan-equipment-table').find('tbody').html(content);
                });
            });
            if(data.penalizacion != null){
                $('#content-penality').show();
                $('#folioPenalizacion').val(data.penalizacion.folio);
                $('#motivo').val(data.penalizacion.motivo);
                $('#totalPenalizacion').val(data.penalizacion.total);
            }else{
                $('#content-penality').hide();
            }
        }
    });
}
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}
function postReturnLoanMaterial(){
    const folio = getUrlParameter('folio');
   $.ajax({
        url : "../../DBphp/prestamoEquipo.php",
        method : "POST",
        data : {folio: folio, action: "loanMaterials"},
        success: function(response){
            var responseParse = jQuery.parseJSON(response);
            const data = responseParse.data;
            var content;
            $('#folio').val(data.folio);
            $('#date').val(data.fecha)
            $('#user').val(data.nombre)
            $('#employee').val(data.empleado)
            $('#user-id').val(data.usuario);
            $.each(data.materiales, function(index, value){
                var numItem = 0;
                numItem = (parseInt(index) + parseInt(1));

                if(value.fecha_devolucion == null){
                    fechaDevolucion = "No se ha entregado";
                    
                } else {
                    fechaDevolucion = value.fecha_devolucion;
                }

                content += '<tr>'+
                        '<th scope="row">'+ numItem +'</th>'+
                        '<td class="text-center"><span></span><input type="hidden" value="'+value.id+'" class="id-book" />'+value.modelo+'</td>'+
                        '<td class="text-center">'+value.tipo+'</td>'+
                        '<td class="text-center">'+value.marca+'</td>'+
                        '<td class="text-center">'+value.estado+'</td>'+
                        '<td class="text-center">'+
                            '<label class="switch">'+
                                '<input  class="devolucion devolucion-check" type="checkbox"'; 
                    if (value.fecha_devolucion != null) {
                        content += ' checked disabled';
                    }
                    content += '/><span class="slider round"></span>'+
                            '</label>'+
                        '</td>'+
                        '<td class="text-center"><span class="return-date">'+fechaDevolucion+'</span></td>'+
                    '</tr>';
                $('#data-loan-material-table').find('tbody').html(content);
            });
            if(data.penalizacion) {
                fillPenality(data.penalizacion);
            }
            getReturnDate();
            checkTotalLoans();
        }
    }); 
}
function fillPenality(penality){
    $('#required-penality').attr('checked', true);
    $('#required-penality').attr('disabled', true);
    $('#content-penality').show();
    $('#folioPenalizacion').val(penality.folio);
    $('#motivo').val(penality.motivo);
    $('#totalPenalizacion').val(penality.total);
}
function checkTotalLoans(){
    var contador = 0;
    var totalChecks =  $('.devolucion-check').length;
    $('.devolucion-check').each(function(index, value){
        if($(this).is(':checked')){
            contador = parseInt(contador) + 1;   
        }
        if(contador == totalChecks){
            $('#required-penality').attr('disabled', true);
            $('.m-body-footer-btns').html('<a href="prestamoEquipo.php" class="btn btn-outline-primary btn-lg btn-block">Regresar</a>')
        }
    });
}

function getReturnDate(){
    $('.devolucion-check').on('change', function(){
        if( $(this).is(':checked') ){
            var date = getDate();
            $(this).closest('tr').find('.return-date').text(date);
        } else {
            $(this).closest('tr').find('.return-date').text("No se ha entregado");
        }
    }); 
}

function getDate(){
    let date = new Date();
    let dd = date.getDate();
    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if(dd < 10){
        dd = '0' + dd;
    }
    if(mm < 10){
        mm = '0' + mm;
    }
    date = yyyy + '-' + mm + '-' + dd;
    return date;
}

function getReturnValues(){
    let materials = [];
    const table = $('#data-loan-material-table');
    table.find('tbody').find('tr').each(function(){
        let returned = $(this).find('.devolucion-check').prop('checked');
        let disabled = $(this).find('.devolucion-check').prop('disabled');
        if(returned && !disabled){
            let materialId = $(this).find('.id-book').val();
            let returnDate = getDate();
            materials.push({'id': materialId, 'returned_date': returnDate});
        }         
    });
    materials = JSON.stringify(materials);
    return materials;
}
function postDataReturnLoanMaterials(){
    $('#submit-form-update-material').on('click', function(){
        let valuesRegistry = getReturnValues();
        let folio = $('#folio').val();
        let userId = $('#user-id').val();
        let folioPenality = $('#folioPenalizacion').val();
        let reasonPenality = $('#motivo').val();
        let totalPenality = $('#totalPenalizacion').val();
        if($('#required-penality').is(':checked')){
            var flagNewFolio = "insert";
            if($('#required-penality').is(':disabled')){
                flagNewFolio = "update";
            }
            console.log(flagNewFolio)
            data = {'action': "update", 'folio': folio, 'materials': valuesRegistry, 'idRequestingUser': userId, 'folioPenality': folioPenality, 'reasonPenality': reasonPenality, 'totalPenality': totalPenality, 'newRegistry': flagNewFolio};
        }else{
            console.log("sin pena")
            data = {'action': "update", 'folio': folio, 'materials': valuesRegistry, 'idRequestingUser': userId};
        }
        $.ajax({
            url : "../../DBphp/prestamoEquipo.php",
            method : "POST",
            data : data,
            success: function(response){
                sweetAlerts(response);
            }
        });  
    });
}

function sweetAlerts(response){
    if(response == "success"){
        swal({
            title: 'Devolución de Material',
            text: "¡Se ha agregado la devolución con exito!",
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
            }).then((result) => {
            if (result.value) {
                window.location.href = 'prestamoEquipo.php';
            }
        }); 
    }
    else{
        swal({
            title: 'Error',
            text: "Ha ocurrido un error al agregar la devolución",
            type: 'error',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar'
        }); 
    }
}

function checkPenality(){
    if ($(this).is(':checked')) {
        $('#content-penality').show();
        folioPenality();
    } else {
        $('#content-penality').hide();
    }
}

function folioPenality(){
    $.ajax({
        url : "../../DBphp/prestamoEquipo.php",
        method : "POST",
        data : {action: "receiveFolioPenality"},
        success: function(response){
            $('#folioPenalizacion').val(response);
        }
    });
}