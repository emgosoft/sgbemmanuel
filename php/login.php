<?php
	session_start();
	include('conexionDB.php');
	if(isset($_SESSION['nombre'])){
		echo '<script> window.location="index.php";</script>';
    }
?>

<html>
    <head>
        <title>Iniciar Sesión</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../css/login.css">
        <!--estilos de iconos Fontawesome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <!--Boostrap estilos-->
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/sweetalert2.min.css">
    </head>
    <body>
            
        <div class="logo">
            <img id="principal-logo" src="../img/logo.jpg" width="140" height="140">
        </div>
        <div class="m-container">
            <form id="form-login" autocomplete="off" method="post" action="validar.php">
                <h3 class="form-title">Login</h3>
                <div class="form-group row">
                    <div class="col-12">
                        <input type="text" class="form-control inpt-form" name="user" id="user" placeholder="Usuario(Email)">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <input type="password" class="form-control inpt-form" name="password" id="password" placeholder="Contraseña">
                    </div>
                </div>
                <div class="col-12 ">
                <div class="col-4" style="margin-left: 27%;margin-top: 10%;">
                    <input class="sub-button" type="submit" id="enviar" value="Iniciar" name="login">
                </div>
            </div>
            </form>
        </div>
        <script type="text/javascript" src="../jquery/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script> 
        <script src="../js/login.js"></script>
        <script type="text/javascript" src="../jquery/jquery.validate.js"></script>
        <script type="text/javascript" src="../jquery/sweetalert2.min.js"></script>
    </body>
</html>