<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
     $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Material</span>
            <input type="hidden" name="action" id="action" value="index">
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Lista de material</span>
                    <a href="createMaterial.php" type="button" class="btn btn-primary btn-plus m-plus-button crear-registros" style="position: relative;left: 42%;">
                        <i class="fas fa-plus m-icon-plus"></i>
                        <span>Nuevo material</span>
                    </a>
                </div>
            </div>
            <div class="col-12">
                <div class="col-10 m-table" id="material-table-container">
                    <table class='table table-bordered table-hover' id='material-table'>
                        <thead>
                            <th>Tipo</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Opciones</th>
                        </thead>
                    </table>
  		        </div>
            </div>
        </div>
    </div>
    <?php include('../footer.php');?>
        <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="viewDataMaterial" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Datos del material</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="" method="get" id="form-update-user">
                    <div class="col-12">
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Tipo</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="typeMaterial" name="tipo" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Marca</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="brandMaterial" name="marca" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Modelo</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="modelMaterial" name="modelo" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Estado</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" name="estado" id="statusMaterial" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/material.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>
