<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <?php include('../../DBphp/material.php');?>
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Material</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Editar material</span>
                </div>
            </div>
            <form action="" method="get" id="form-update-material">
                <div class="col-12" style="margin-bottom: 5%;">
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Tipo</label>
                            </div>
                            <div class="col">
                                <input type="hidden" name="action" id="action" value="update">
                                <input type="hidden" name="id" value="<?php echo $idMaterial?>">
                                <input class="form-control" type="text" name="tipo" value="<?php echo $tipo?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Marca</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="marca" value="<?php echo $marca?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Modelo</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="modelo" value="<?php echo $modelo?>"  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Estado</label>
                            </div>
                            <div class="col">
                                <input class="form-control" name="estado" id="statusMaterial" value="<?php echo $estado?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                    <a class="btn btn-outline-danger" href="material.php" >Cancelar</a>
                <input class="btn btn-primary" id="submit-form-update-material" type="button" value="Guardar">
            </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/material.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>