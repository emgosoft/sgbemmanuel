<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Material</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Crear material</span>
                </div>
            </div>
            <form action="" method="post" id="form-create-material">
                <div class="col-12" style="margin-bottom: 3%;">
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Tipo</label>
                            </div>
                            <div class="col">
                                <input type="hidden" name="action" id="action" value="create">
                                <input class="form-control" type="text" id="typeMaterial" name="tipo" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Marca</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" id="brandMaterial" name="marca" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Modelo</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" id="modelMaterial" name="modelo" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Estado</label>
                            </div>
                            <div class="col">
                                <textarea class="form-control" name="estado" id="statusMaterial"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                    <a class="btn btn-outline-danger" href="material.php" >Cancelar</a>
                    <input class="btn btn-primary " id="btn-create-material" type="button" value="Enviar">
                </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/material.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>