<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Reportes</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title">
                <div class="col-12 m-head" style="margin-bottom: 4% !important;">
                    <span class="sub-title-page">Generar reportes</span>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a class="col-3 container-reports" href="../../DBphp/reportes/reportesUsuarios.php" target="_blank">
                        <span class="text-report">
                            Usuarios
                            <br>
                            <i class="fas fa-file-invoice icon-report"></i>
                        </span>
                    </a>
                    <a class="col-3 container-reports" href="../../DBphp/reportes/reporteLibros.php" target="_blank">
                        <span class="text-report">
                            Libros
                            <br>
                            <i class="fas fa-file-invoice icon-report"></i>
                        </span>
                    </a>
                    <a class="col-3 container-reports" href="../../DBphp/reportes/reporteMaterial.php" target="_blank">
                        <span class="text-report">
                            Material
                            <br>
                            <i class="fas fa-file-invoice icon-report"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/reportes.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>
