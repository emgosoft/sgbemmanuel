<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Devolución Material</span>
        </div>
        <div class="col-11 m-content-sub" style="padding: 3rem !important;">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Devolución Material</span>
                </div>
            </div>
            <form action="" method="get" id="form-return-loan-material">
                <div class="col-12" style="margin-bottom: 3%;margin-top: 3%;">
                    <div class="row">
                        <div class="col-6 text-center">
                            <div class="row row-form">
                                <div class="col-12">
                                    <label>Folio</label>
                                    <input type="hidden" id="view" value="loanMaterial">
                                    <input class="form-control" type="text" id="folio" style="text-align:center;" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 text-center">
                            <div class="row row-form">
                                <div class="col-12">
                                    <label>Fecha creación</label>
                                    <input class="form-control" type="text" id="date" autocomplete="off" style="text-align:center;" disabled="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 text-center">
                            <div class="row row-form">
                                <div class="col-12">
                                    <label>Usuario</label>
                                    <input class="form-control" type="text" id="user" autocomplete="off" style="text-align:center;" disabled="true">
                                    <input type="hidden" id="user-id">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 text-center">
                            <div class="row row-form">
                                <div class="col-12">
                                    <label>Empleado</label>
                                    <input class="form-control" type="text" id="employee" autocomplete="off" style="text-align:center;" disabled="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <br><hr>
                            <h5>Material</h5>
                            <br>
                            <table class="table table-striped" id="data-loan-material-table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th class="text-center">Modelo</th>
                                        <th class="text-center">Tipo</th>
                                        <th class="text-center">Marca</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Devolución</th>
                                        <th class="text-center">Fecha Devolución</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <br><hr>
                        </div>
                        <div class="col-12" style="padding: 5px;padding-top: 18px;background-color: #fafafa;">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-3" style="text-align: right;">
                                            <label class="font-weight-bold">Requiere penalización</label>
                                        </div>
                                        <div class="col-2" style="text-align: left;">
                                            <label class="switch">
                                                <input type="checkbox"  id="required-penality">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" id="content-penality">
                            <hr>
                            <h5>Penalización</h5>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <div class="row row-form">
                                        <div class="col-3">
                                            <label>Folio</label>
                                        </div>
                                        <div class="col-9">
                                            <input class="form-control" type="text" id="folioPenalizacion" style="text-align:center;" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <div class="row row-form">
                                        <div class="col-3">
                                            <label>Motivo</label>
                                        </div>
                                        <div class="col-9">
                                            <textarea rows="5" cols="50" class="form-control" type="text" id="motivo"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-center" style="margin: 0px 0px 10px 0px;">
                                    <div class="row row-form">
                                        <div class="col-8" style="text-align: right;">
                                            <label>Total a cobrar</label>
                                        </div>
                                        <div class="col-4">
                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                <div class="input-group-addon">$</div>
                                                <input  id="totalPenalizacion" class="form-control" type="text" style="text-align:center;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                    <a class="btn btn-outline-danger" href="prestamoEquipo.php" >Cancelar</a>
                    <input class="btn btn-primary" id="submit-form-update-material" type="button" value="Guardar">
                </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/prestamoEquipo.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>