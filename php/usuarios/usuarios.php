<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
     $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Usuarios</span>
            <input type="hidden" id="action" value="index">
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Lista de Usuarios</span>
                    <a href="createUser.php" type="button" class="btn btn-primary add-product btn-plus m-plus-button crear-registros" style="position: relative;left: 42%;">
                        <i class="fas fa-plus m-icon-plus"></i>
                        <span>Nuevo usuario</span>
                    </a>
                </div>
            </div>
            <div class="col-12">
                <div class="col-10 m-table" id="user-table-container">
                    <table class='table table-bordered table-hover' id='users-table'>
                        <thead>
                            <th>Nombre</th>
                            <th>Numero Celular</th>
                            <th>E-mail</th>
                            <th>Area</th>
                            <th>Opciones</th>
                        </thead>
                    </table>
  		        </div>
            </div>
        </div>
    </div>
    <?php include('../footer.php');?>
        <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="viewDataUser" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Datos del usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="" method="get" id="form-update-user">
                    <div class="col-12">
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Nombre</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="nameUser" name="name" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Numero Celular</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="telephoneUser" name="numero_celular" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Contraseña</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="passwordUser" name="contraseña" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Genero</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="genderUser" name="genero" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>E-mail</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="emailUser" name="correo" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label># Libros Prestados</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="borrowedBooksUser" name="libros_prestados" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Estatus</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="statusUser"  name="estatus" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Tipo</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="typeUser" name="tipo" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Area</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="areaUser" name="area" value="" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/usuarios.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>