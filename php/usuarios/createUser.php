<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Usuarios</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Crear usuario</span>
                </div>
            </div>
            <form action="" method="post" id="form-create-user">
                <div class="col-12" style="margin-bottom: 5%;">
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Nombre</label>
                            </div>
                            <div class="col">
                                <input type="hidden" name="action" value="create" id="action">
                                <input class="form-control" id="nombre" type="text" name="name" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Numero Celular</label>
                            </div>
                            <div class="col">
                                <input class="form-control" id="numero_celular" type="text" name="numero_celular" value=""  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Contraseña</label>
                            </div>
                            <div class="col">
                                <input class="form-control" id="contraseña" type="text" name="contraseña" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Genero</label>
                            </div>
                            <div class="col">
                                <select class="form-control" name="genero" id="genero">
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>E-mail</label>
                            </div>
                            <div class="col">
                                <input class="form-control" id="correo" type="text" name="correo" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label># Libros Prestados</label>
                            </div>
                            <div class="col">
                                <input class="form-control" id="libros_prestados" type="text"  value="0" autocomplete="off" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Estatus</label>
                            </div>
                            <div class="col">
                                <select class="form-control" name="estatus" id="estatus">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Tipo</label>
                            </div>
                            <div class="col">
                                <select class="form-control" id="tipo-usuario" name="tipo">
                                    <option value="Estudiante">Estudiante</option>
                                    <option value="Docente">Docente</option>
                                    <option value="Bibliotecario">Bibliotecario</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Area</label>
                            </div>
                            <div class="col">
                                <input type="text" id="area" name="area" class="form-control" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                    <a class="btn btn-outline-danger" href="usuarios.php" >Cancelar</a>
                    <input class="btn btn-primary " id="btn-create-user" type="button" value="Enviar">
                </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/usuarios.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>