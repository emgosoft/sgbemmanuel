<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
     $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <?php include('../../DBphp/usuarios.php');?>
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Usuarios</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Editar Usuario</span>
                </div>
            </div>
            <form action="" method="get" id="form-update-user">
                <div class="col-12" style="margin-bottom: 5%;">
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Nombre</label>
                            </div>
                            <div class="col">
                                <input type="hidden" name="action" value="update" id="action">
                                <input type="hidden" name="id" value="<?php echo $idUser?>">
                                <input class="form-control" type="text" name="nombre" value="<?php echo $nombre?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Numero Celular</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="numero_celular" value="<?php echo $numCelular?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Contraseña</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="contraseña" value="<?php echo $password?>"  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Genero</label>
                            </div>
                            <div class="col">
                                <select class="form-control" name="genero" id="">
                                    <option value="<?php echo $genero?>"><?php echo $genero?></option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Femenino</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>E-mail</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="correo" value="<?php echo $correo?>"  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label># Libros Prestados</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="libros_prestados" value="<?php echo $librosPrestados?>" autocomplete="off" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Estatus</label>
                            </div>
                            <div class="col">
                                <select class="form-control" name="estatus" id="">
                                    <option value="<?php echo $estatus?>"><?php echo $estatusText?></option>
                                    <option value="0">Inactivo</option>
                                    <option value="1">Activo</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Tipo</label>
                            </div>
                            <div class="col">
                                <select class="form-control" name="tipo" id="">
                                    <option value="<?php echo $tipo?>"><?php echo $tipo?></option>
                                    <option value="Estudiante">Estudiante</option>
                                    <option value="Docente">Docente</option>
                                    <option value="Bibliotecario">Bibliotecario</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Area</label>
                            </div>
                            <div class="col">
                                <input type="text" name="area" class="form-control" value="<?php echo $area?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                <a class="btn btn-outline-danger" href="usuarios.php" >Cancelar</a>
                <input class="btn btn-primary" id="submit-form-update-user" type="button" value="Guardar">
            </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/usuarios.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>