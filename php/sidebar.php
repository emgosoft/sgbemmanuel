<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../../css/sidebar.css">
        <!--estilos de iconos Fontawesome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <!--Boostrap estilos-->
        <link rel="stylesheet" type="text/css" href="../../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../../css/tablas.css">
        <link rel="stylesheet" href="../../css/general.css">
        <link rel="stylesheet" type="text/css" href="../../css/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="../../css/modal.css">
        <!-- select2 css -->
        <link rel="stylesheet" type="text/css" href="../../css/select2.min.css">
        <!-- dateTimePicker -->
        <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
    </head>
    <body>
        <div class="wrapper">
            <!-- Sidebar -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <img src="../../img/logo.jpg" width="80" height="80" style="margin-left: 28%;">
                </div>
                <ul class="list-unstyled components">
                    <p>Componetes</p>
                    <li id="li-usuarios">
                        <a class="extern-page" href="../usuarios/usuarios.php"><i class="fas fa-user" style="margin-right: 8%;"></i>Usuarios</a>
                    </li>
                    <li id="principal-page-index" class="active-li">
                        <a class="extern-page" href="../libros/libros.php"><i class="fas fa-book" style="margin-right: 8%;"></i>Libros</a>
                    </li>
                    <li id="li-material">
                        <a class="extern-page" href="../material/material.php"><i class="fas fa-laptop"  style="margin-right: 8%;"></i>Material</a>
                    </li>
                    <li>
                        <a href="#homeSubmenu" data-toggle="collapse" id="a-prestamos" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-calendar-check" style="margin-right: 8%;"></i><span>Préstamos</span></a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a class="extern-page" href="../prestamoLibros/prestamoLibros.php">Material Bibliográfico</a>
                            </li>
                            <li class="li-equipo-computo">
                                <a class="extern-page" href="../prestamoMaterial/prestamoEquipo.php">Equipo de computo</a>
                            </li>
                        </ul>
                    </li>
                    <li id="li-reportes">
                        <a class="extern-page" href="../reportes/reportes.php"><i class="fas fa-file-alt" style="margin-right: 8%;"></i>Reportes</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="container-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="header col-12">
                        <ul id="header-options">
                            <li class="hd-option">
                                <img id="user-options" class="img-hd" src="../../img/user.png" width="45" height="45">
                                <div class="col-12 opt-sesion" style="padding:0;">
                                    <div class="pointer-tooltip"></div>
                                        <div class="opt-sesion-header">
                                            <span id="user-name"><?php echo $userName;?></span>
                                            <br>
                                            <span id="user-area"><?php echo $area;?></span>
                                            <br>
                                            <span id="user-type">Tipo: <?php echo $tipo;?></span>
                                            <input type="hidden" id="id" value="<?php echo $id;?>">
                                            <input type="hidden" id="tipo" value="<?php echo $tipo;?>">
                                        </div>
                                        <div class="opt-sesion-options">
                                            <a href="../logout.php" id="btn-close-sesion">Cerrar sesión</a>
                                        </div>
                                    </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row m-body">
                