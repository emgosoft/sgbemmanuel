<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <?php include('../../DBphp/libros.php');?>
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Libros</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Editar Libros</span>
                </div>
            </div>
            <form action="" method="get" id="form-update-book">
                <div class="col-12" style="margin-bottom: 5%;">
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Titulo</label>
                            </div>
                            <div class="col">
                                <input type="hidden" name="action" id="action" value="update">
                                <input type="hidden" name="id" value="<?php echo $idBook?>">
                                <input class="form-control" type="text" name="titulo" value="<?php echo $titulo?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Autor</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="autor" value="<?php echo $autor?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Editorial</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="editorial" value="<?php echo $editorial?>"  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>ISBN</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="isbn" value="<?php echo $isbn?>"  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Año de edición</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="anio_edicion" value="<?php echo $anioEdicion?>"  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Genero</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="genero" value="<?php echo $genero?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Disponible</label>
                            </div>
                            <div class="col">
                                <select class="form-control" name="disponible" id="">
                                    <option value="<?php echo $disponible?>"><?php echo $disponibleText?></option>
                                    <option value="0">No hay disponibles</option>
                                    <option value="1">Hay disponibles</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Existencia</label>
                            </div>
                            <div class="col">
                            <input class="form-control" type="text" name="existencia" value="<?php echo $existencia?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                    <a class="btn btn-outline-danger" href="libros.php" >Cancelar</a>
                    <input class="btn btn-primary" id="submit-form-update-book" type="button" value="Guardar">
                </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/libros.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>