<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
     $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Libros</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Crear libro</span>
                </div>
            </div>
            <form action="" method="post" id="form-create-book">
                <div class="col-12" style="margin-bottom: 5%;">
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Titulo</label>
                            </div>
                            <div class="col">
                                <input type="hidden" name="action" id="action" value="create">
                                <input class="form-control" type="text" name="titulo" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Autor</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="autor" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Editorial</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="editorial" value=""  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>ISBN</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="isbn" value=""  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Año de edición</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="anio_edicion" value=""  autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Genero</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="genero" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Disponible</label>
                            </div>
                            <div class="col">
                                <select class="form-control" name="disponible" id="">
                                    <option value="0">No hay disponibles</option>
                                    <option value="1">Hay disponibles</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-4 col-md-2">
                                <label>Existencia</label>
                            </div>
                            <div class="col">
                                <input class="form-control" type="text" name="existencia" value="" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                    <a class="btn btn-outline-danger" href="libros.php" >Cancelar</a>
                    <input class="btn btn-primary " id="btn-create-book" type="button" value="Enviar">
                </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/libros.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>