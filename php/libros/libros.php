<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Libros</span>
            <input type="hidden" name="action" id="action" value="index">
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Lista de Libros</span>
                    <a href="createLibro.php" type="button" class="btn btn-primary add-product btn-plus m-plus-button crear-registros" style="position: relative;left: 42%;">
                        <i class="fas fa-plus m-icon-plus"></i>
                        <span>Nuevo Libro</span>
                    </a>
                </div>
            </div>
            <div class="col-12">
                <div class="col-10 m-table" id="book-table-container">
                    <table class='table table-bordered table-hover' id='book-table'>
                        <thead>
                            <th>Titulo</th>
                            <th>Autor</th>
                            <th>Editorial</th>
                            <th>Genero</th>
                            <th>Existencia</th>
                            <th>Opciones</th>
                        </thead>
                    </table>
  		        </div>
            </div>
        </div>
    </div>
    <?php include('../footer.php');?>
        <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="viewDataBook" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Datos del libro</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form action="" method="get" id="form-update-book">
                    <div class="col-12">
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Titulo</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="tituloLibro" name="titulo" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Autor</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="autorLibro" name="autor" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Editorial</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="editorialLibro" name="editorial" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>ISBN</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="isbnLibro" name="isbn" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Año de edición</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="anioEdicionLibro" name="anio_edicion" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Genero</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="generoLibro" name="genero" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Disponible</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="disponibleLibro"  name="disponible" value="" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row row-form">
                                <div class="col-4 col-md-2">
                                    <label>Existencia</label>
                                </div>
                                <div class="col">
                                    <input class="form-control" type="text" id="existenciaLibro" name="existencia" value="" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/libros.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>