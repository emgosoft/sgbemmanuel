<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Préstamo | Bibliografico</span>
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title" style="height: 45px;">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Generar préstamo bibliografico</span>
                    <input type="hidden" name="" value="create" id="view">
                </div>
            </div>
            <div class="col-12" style="margin-bottom: 4%; margin-top: 3%;">
                <div class="row">
                    <div class="col-4">
                        <div class="row row-form">
                            <div class="col-12">
                                <label>Folio</label>
                                <input class="form-control" type="text" id="folio" autocomplete="off" style="text-align:center;" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row row-form">
                            <div class="col-12">
                                <label>Fecha</label>
                                <input class="form-control" type="text" id="date" name="date" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row row-form">
                            <div class="col-12">
                                <label>Usuario</label> <br>
                                <select name="idUser" id="idRequestingUser" class="form-control" autocomplete="off">
                                    <option value="">Selecciona una opcion</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row row-form">
                            <div class="col-12">
                                <span id="total-borrowed-books-title" class="col-12"></span>
                            </div>
                        </div>
                    </div>
                    <input class="form-control" type="hidden" id="idUserLoan" name="idUserLoan" value="<?php echo $id;?>">
                </div>
                <br>
            <form action="" method="post" id="form-create-loan-book">
                    <div class="table-repsonsive">
                        <table class="table table-hover" id="loan-books-table">
                            <tr>
                                <th>Folio</th>
                                <th>Libro</th>
                                <th>Extension</th>
                                <th><button type="button" name="add" class="btn btn-success btn-sm add-registry"><span style="font-size: 20px;font-weight: 900; margin: 5px;">+</span></button></th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-12 m-body-footer-btns">
                    <a class="btn btn-outline-danger" href="prestamoLibros.php" >Cancelar</a>
                    <input class="btn btn-primary " id="btn-create-loan-book" type="button" value="Enviar">
                </div>
            </form>
        </div>
    </div>
    <?php include('../footer.php');?>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/prestamoLibros.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>