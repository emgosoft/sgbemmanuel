<?php
session_start();
if(isset($_SESSION['correo'])){ ?>
<?php
    $id = $_SESSION['id'];
    $userName = $_SESSION['correo'];
    $area = $_SESSION['area'];
    $tipo = $_SESSION['tipo'];
    include('../sidebar.php');
?>  
    <div class="col-12 m-content">
        <div class="col-12">
            <span class="title-page">Préstamos | Material Bibliográfico</span>
            <input type="hidden" id="view" value="index">
        </div>
        <div class="col-11 m-content-sub">
            <div class="col-12 content-title">
                <div class="col-12 m-head">
                    <span class="sub-title-page">Lista de Préstamo Bibliográfico</span>
                    <a href="createPrestamoLibros.php" type="button" class="btn btn-primary add-product btn-plus m-plus-button crear-registros" style="position: relative;left: 42%;">
                        <i class="fas fa-plus m-icon-plus"></i>
                        <span>Nuevo</span>
                    </a>
                </div>
            </div>
            <div class="col-12">
                <div class="col-10 m-table" id="loan-books-table-container">
                    <table class='table table-bordered table-hover' id='loan-books-table'>
                        <thead>
                            <th>Folio</th>
                            <th>Fecha de creación</th>
                            <th>Usuario</th>
                            <th># Libros</th>
                            <th>Opciones</th>
                        </thead>
                    </table>
  		        </div>
            </div>
        </div>
    </div>
    <?php include('../footer.php');?>
        <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="viewDataLoanBook" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Datos del prestamo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form action="" method="get" id="">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6 text-center">
                                    <div class="row row-form">
                                        <div class="col-12">
                                            <label>Folio</label>
                                            <input class="form-control" type="text" id="folio" style="text-align:center;" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 text-center">
                                    <div class="row row-form">
                                        <div class="col-12">
                                            <label>Fecha creación</label>
                                            <input class="form-control" type="text" id="date" autocomplete="off" style="text-align:center;" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 text-center">
                                    <div class="row row-form">
                                        <div class="col-12">
                                            <label>Usuario</label>
                                            <input class="form-control" type="text" id="user" autocomplete="off" style="text-align:center;" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 text-center">
                                    <div class="row row-form">
                                        <div class="col-12">
                                            <label>Empleado</label>
                                            <input class="form-control" type="text" id="employee" autocomplete="off" style="text-align:center;" disabled="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <br><hr>
                                    <h5>Libros</h5>
                                    <br>
                                    <table class="table table-striped" id="data-loan-books-table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th class="text-center">Titulo</th>
                                                <th class="text-center">Genero</th>
                                                <th class="text-center">ISBN</th>
                                                <th class="text-center">Extensión</th>
                                                <th class="text-center">Fecha devolución</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="col-12" id="content-penality">
                                    <hr>
                                        <h5>Penalización</h5>
                                        <div class="row">
                                            <div class="col-12 text-center">
                                                <div class="row row-form">
                                                    <div class="col-3">
                                                        <label>Folio</label>
                                                    </div>
                                                    <div class="col-9">
                                                        <input class="form-control" type="text" id="folioPenalizacion" style="text-align:center;" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 text-center">
                                                <div class="row row-form">
                                                    <div class="col-3">
                                                        <label>Motivo</label>
                                                    </div>
                                                    <div class="col-9">
                                                        <textarea rows="5" cols="50" class="form-control" type="text" id="motivo" disabled></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 text-center" style="margin: 0px 0px 10px 0px;">
                                                <div class="row row-form">
                                                    <div class="col-8" style="text-align: right;">
                                                        <label>Total</label>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                            <div class="input-group-addon">$</div>
                                                            <input  id="totalPenalizacion" class="form-control" type="text" style="text-align:center;" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../js/prestamoLibros.js"></script>
    <?php include('../end.php'); ?>
<?php  
}else{
    echo '<script>window.location="../login.php";</script>';
}
?>