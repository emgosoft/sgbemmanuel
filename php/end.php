
        <script type="text/javascript" src="../../bootstrap/js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="../../js/general.js"></script>
        <script type="text/javascript" src="../../jquery/sweetalert2.min.js"></script>
        <script type="text/javascript" src="../../jquery/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../../jquery/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="../../jquery/jquery.validate.js"></script>
        <script type="text/javascript" src="../../jquery/select2.min.js"></script>
        <script type="text/javascript" src="../../jquery/jquery-ui.js"></script>
        <script type="text/javascript" src="../../js/repeater.js"></script>
        <?php
            if(isset($_SESSION['tipo'])){
                if($_SESSION['tipo'] == "Estudiante"){
                    echo '<script>
                        $(\'#li-usuarios\').hide();
                        $(\'.crear-registros\').removeAttr("href");
                        $(\'.btn-edit-data\').remove();
                        $(\'#li-material\').hide();
                        $(\'#li-reportes\').hide();
                        $(\'.li-equipo-computo\').hide();
                        $(\'#a-prestamos\').find(\'span\').html("Mis préstamos y devoluciones");
                    </script>';
                }else if($_SESSION['tipo'] == "Docente"){
                    echo '<script>
                        $(\'#li-usuarios\').hide();
                        $(\'.crear-registros\').removeAttr("href");
                        $(\'.btn-edit-data\').remove();
                        $(\'#li-reportes\').hide();
                        $(\'#a-prestamos\').find(\'span\').html("Mis préstamos y devoluciones");
                    </script>';
                }
            }
        ?>
    </body>
</html>