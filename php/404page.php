<html>
    <head>
    <link rel="stylesheet" href="../css/404page.css">
    </head>
    <body>
    <div class="cont_principal">
        <div class="cont_error">
            <h1>Oops</h1>  
            <p>Error al ingresar los datos</p>
            <a class="btn-redirect" href="login.php">Regresar al login</a>
        </div>
        <div class="cont_aura_1"></div>
        <div class="cont_aura_2"></div>
    </div>
    <script>
        window.onload = function(){
            document.querySelector('.cont_principal').className= "cont_principal cont_error_active";  
        }
    </script>
    </body>
</html>