<?php
    if(isset($_POST['action'])){//obtener vista de ejecucion
        include('../php/conexionDB.php');
        $action = $_POST['action'];
        principal($dbconex, $action);
    }
    if(isset($_GET['idBook'])){
        include('../../php/conexionDB.php');
        $idBook = $_GET['idBook'];
        list($titulo,
            $autor,
            $editorial,
            $isbn,
            $anioEdicion,
            $genero,
            $disponibleText,
            $existencia) = getDataBookUpdate($dbconex, $idBook);
    }

    function principal($dbconex, $action){//funcion principal
        switch($action){
            case "index":
                postDataViewIndex($dbconex);
                break;
            case "create":
                createBook($dbconex);
                break;
            case "view":
                postDataViewBook($dbconex);
                break;
            case "update":
                postUpdateBook($dbconex);
                break;
            default: echo "error"; break;
        }
    }

    function postDataViewIndex($dbconex){
        $return_arr = array();
        $tabla = $dbconex->query("SELECT * FROM libros");
        if(!$tabla){
            echo mysqli_errors($dbconex);
        }else{
            while($row = mysqli_fetch_array($tabla)){
                $id = $row['id'];
                $titulo = $row['titulo'];
                $autor = $row['autor'];
                $editorial = $row['editorial'];
                $genero = $row['genero'];
                $existencia = $row['existencia'];
    
                $return_arr[] = array("id"=>$id,
                                    "titulo"=>$titulo,
                                    "autor"=>$autor,
                                    "editorial"=>$editorial,
                                    "genero"=>$genero,
                                    "existencia"=>$existencia);
            }
            echo json_encode($return_arr);
        }
        $dbconex->close();
    }

    function postDataViewBook($dbconex){
        $return_arr = array();
        if(isset($_POST['idBook'])){
            $idBook = $_POST['idBook'];
        }
        $consult = "SELECT * FROM libros WHERE id = '$idBook'";
        $datos = $dbconex->query($consult);
        if(!$datos){
            echo mysqli_errors($dbconex);
        }else{
            while($row = mysqli_fetch_array($datos)){
                $titulo = $row['titulo'];
                $autor = $row['autor'];
                $editorial = $row['editorial'];
                $isbn = $row['isbn'];
                $anioEdicion = $row['anio_edicion'];
                $genero = $row['genero'];
                $disponible = $row['disponible'];
                $existencia = $row['existencia'];
        
                $return_arr[] = array("titulo"=>$titulo,
                                    "autor"=>$autor,
                                    "editorial"=>$editorial,
                                    "isbn"=>$isbn,
                                    "anio_edicion"=>$anioEdicion,
                                    "genero"=>$genero,
                                    "disponible"=>$disponible,
                                    "existencia"=>$existencia);
            }
            echo json_encode($return_arr);
        }
        $dbconex->close();
    }

    function createBook($dbconex){
        if (isset($_POST['titulo'])) {
            $titulo = $_POST['titulo'];
        }
        if (isset($_POST['autor'])) {
            $autor = $_POST['autor'];
        }
        if (isset($_POST['editorial'])) {
            $editorial = $_POST['editorial'];
        }
        if (isset($_POST['isbn'])) {
            $isbn = $_POST['isbn'];
        }
        if (isset($_POST['anio_edicion'])) {
            $anioEdicion = $_POST['anio_edicion'];
        }
        if (isset($_POST['genero'])) {
            $genero = $_POST['genero'];
        }
        if (isset($_POST['disponible'])) {
            $disponible = $_POST['disponible'];
        }
        if (isset($_POST['existencia'])) {
            $existencia = $_POST['existencia'];
        }
        $consult =  $dbconex->query("INSERT INTO libros (titulo,autor,editorial,isbn,anio_edicion,genero,disponible,existencia) VALUES('$titulo','$autor','$editorial','$isbn','$anioEdicion','$genero','$disponible','$existencia') ");
        if(!$consult){
            echo mysqli_errors($dbconex);
            echo "error";
        }else{
            echo "success";
        }
    }

    function getDataBookUpdate($dbconex, $idBook){
        $consult = "SELECT * FROM libros WHERE id = '$idBook'";    
        $datos = $dbconex->query($consult);
        if(!$datos){
            echo mysqli_errors($dbconex);
        }else{
            $fila = mysqli_fetch_array($datos);
            $titulo = $fila['titulo'];
            $autor = $fila['autor'];
            $editorial = $fila['editorial'];
            $isbn = $fila['isbn'];
            $anioEdicion = $fila['anio_edicion'];
            $genero = $fila['genero'];
            $disponible = $fila['disponible'];
            $existencia = $fila['existencia'];
            
            if($disponible == 1)
                $disponibleText = "Hay disponibles";
            else if($disponible == 0)
                $disponibleText = "No hay disponibles";
            
            return array($titulo,$autor,$editorial,$isbn,$anioEdicion,$genero,$disponibleText,$existencia); 
        }
        $dbconex->close();
    }

    function postUpdateBook($dbconex){
        if(isset($_POST['id'])){
            $idBook = $_POST['id'];
        }
        if (isset($_POST['titulo'])) {
           $titulo = $_POST['titulo'];
       }
       if (isset($_POST['autor'])) {
           $autor = $_POST['autor'];
       }
       if (isset($_POST['editorial'])) {
           $editorial = $_POST['editorial'];
       }
       if (isset($_POST['isbn'])) {
           $isbn = $_POST['isbn'];
       }
       if (isset($_POST['anio_edicion'])) {
           $anioEdicion = $_POST['anio_edicion'];
       }
       if (isset($_POST['genero'])) {
           $genero = $_POST['genero'];
       }
       if (isset($_POST['disponible'])) {
           $disponible = $_POST['disponible'];
       }
       if (isset($_POST['existencia'])) {
           $existencia = $_POST['existencia'];
       }
       $sentencia = "UPDATE libros SET titulo = '$titulo', autor='$autor', editorial='$editorial', isbn='$isbn', anio_edicion='$anioEdicion', genero = '$genero',disponible = '$disponible', existencia = '$existencia' WHERE id = '".$idBook."'";
       $result =  $dbconex->query($sentencia);
       if(!$result){
           echo mysqli_errors($dbconex);
           echo "error";
       }else{
           echo "success";
       }
    }
?>