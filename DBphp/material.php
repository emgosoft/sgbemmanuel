<?php
    if(isset($_POST['action'])){//obtener vista de ejecucion
        include('../php/conexionDB.php');
        $action = $_POST['action'];
        principal($dbconex, $action);
    }
    if(isset($_GET['idMaterial'])){
        include('../../php/conexionDB.php');
        $idMaterial = $_GET['idMaterial'];
        list($idMaterial,$tipo,$marca,$modelo,$estado) = getDataMaterialUpdate($dbconex, $idMaterial);
    }

    function principal($dbconex, $action){//funcion principal
        switch($action){
            case "index":
                postDataViewIndex($dbconex);
                break;
            case "create":
                createMaterial($dbconex);
                break;
            case "view":
                postDataViewMaterial($dbconex);
                break;
            case "update":
                postUpdateMaterial($dbconex);
                break;
            default: echo "error"; break;
        }
    }
    
    function postDataViewIndex($dbconex){
        $return_arr = array();
        $tabla = $dbconex->query("SELECT * FROM material");
        if(!$tabla){
            echo mysqli_errors($dbconex);
        }else{
            while($row = mysqli_fetch_array($tabla)){
                $id = $row['id'];
                $tipo = $row['tipo'];
                $marca = $row['marca'];
                $modelo = $row['modelo'];
        
                $return_arr[] = array("id"=>$id,
                                    "tipo"=>$tipo,
                                    "marca"=>$marca,
                                    "modelo"=>$modelo);
            }
            echo json_encode($return_arr);
        }
        $dbconex->close();
    }

    function postDataViewMaterial($dbconex){
        $return_arr = array();
        if(isset($_POST['idMaterial'])){
            $idMaterial = $_POST['idMaterial'];
        }
        $consulta = "SELECT * FROM material WHERE id = '$idMaterial'";
        $datos = $dbconex->query($consulta);
        if(!$datos){
            echo mysqli_errors($dbconex);
        }else{
            while($row = mysqli_fetch_array($datos)){
                $tipo = $row['tipo'];
                $marca = $row['marca'];
                $modelo = $row['modelo'];
                $estado = $row['estado'];
        
                $return_arr[] = array("tipo"=>$tipo,
                                    "marca"=>$marca,
                                    "modelo"=>$modelo,
                                    "estado"=>$estado);
            }
            echo json_encode($return_arr);   
        }
        $dbconex->close();
    }
    
    function createMaterial($dbconex){
        if (isset($_POST['tipo'])) {
            $tipo = $_POST['tipo'];
        }
        if (isset($_POST['marca'])) {
            $marca = $_POST['marca'];
        }
        if (isset($_POST['modelo'])) {
            $modelo = $_POST['modelo'];
        }
        if (isset($_POST['estado'])) {
            $estado = $_POST['estado'];
        }
        $result = $dbconex->query("INSERT INTO material (tipo,marca,modelo,estado) VALUES('$tipo','$marca','$modelo','$estado') ");
        if(!$result){
            echo mysqli_errors($dbconex);
            echo "error";
        }else{
            echo "success";
        }
        $dbconex->close();
    }

    function getDataMaterialUpdate($dbconex, $idMaterial){
        $consult = "SELECT * FROM material WHERE id = '$idMaterial'";
        $datos = $dbconex->query($consult);
        if(!$datos){
            echo mysqli_errors($dbconex);
        }else{
            $fila = mysqli_fetch_array($datos);
            $tipo = $fila['tipo'];
            $marca = $fila['marca'];
            $modelo = $fila['modelo'];
            $estado = $fila['estado'];
            return array($idMaterial,$tipo,$marca,$modelo,$estado);
        }
        $dbconex->close();
    }

    function postUpdateMaterial($dbconex){
        if(isset($_POST['id'])){
            $idMaterial = $_POST['id'];
        }
        if (isset($_POST['tipo'])) {
           $tipo = $_POST['tipo'];
        }
        if (isset($_POST['marca'])) {
           $marca = $_POST['marca'];
        }
        if (isset($_POST['modelo'])) {
           $modelo = $_POST['modelo'];
        }
        if (isset($_POST['estado'])) {
           $estado = $_POST['estado'];
        }
       $sentencia = "UPDATE material SET tipo = '$tipo', marca='$marca', modelo='$modelo', estado='$estado' WHERE id = '".$idMaterial."'";
       $result =  $dbconex->query($sentencia);
       if(!$result){
           echo mysqli_errors($dbconex);
           echo "error";
       }else{
           echo "success";
       }     
    }
?>