<?php
    include('../php/conexionDB.php');
    
    if(isset($_POST['action'])){//obtener vista de ejecucion
        $action = $_POST['action'];
        principal($dbconex, $action);
    }

    function principal($dbconex, $action){//funcion principal
        if(isset($_POST['idUser'])){
            $idUser = $_POST['idUser'];
        }
        if(isset($_POST['typeUser'])){
            $typeUser = $_POST['typeUser'];
        }
        if(isset($_POST['idUserBorrowedBooks'])){
            $idUserBorrowedBooks = $_POST['idUserBorrowedBooks'];
        }
        switch($action){
            case "index":
                postDataViewIndex($typeUser, $idUser, $dbconex);
                break;
            case "create":
                createLoanBook($dbconex);
                break;
            case "fillFolio":
                postFolio($dbconex);
                break;
            case "fillSelect":
                postUserSelect($dbconex);
                break;
            case "borrowedBooks":
                postBorrowedBooks($dbconex, $idUserBorrowedBooks);
                break; 
            case "fillBooksRepeater":
                postBooksRepeater($dbconex);
                break;
            case "view":
                postDataLoanBook($dbconex);
                break;
            case "loanBooks":
                returnLoanBook($dbconex);
                break;
            case "update":
                updateBorrowedBooks($dbconex);
                break;
            case "receiveFolioPenality":
                postGetFolioPenality($dbconex);
                break;
            default: echo "error"; break;
        }
    }

    function postDataViewIndex($typeUser, $idUser, $dbconex){//enviar datos a la vista index
        if($typeUser != "Bibliotecario"){
            postDataUserTypeNoBibliotecario($typeUser, $idUser, $dbconex);
        }else{//todos los registros
            postDataUserTypeBibliotecario($dbconex);
        }
        $dbconex->close();
    }
    
    function postDataUserTypeNoBibliotecario($typeUser, $idUser, $dbconex){//vista index: mostrar prestamos por Usuario
        $return_arr = [];
        $queryFolios = "SELECT DISTINCT folio FROM detalle_libro";
        $retval = $dbconex->query($queryFolios);
        if(!$retval){
            echo mysqli_error($dbconex);
        }else{
            if (mysqli_num_rows($retval) > 0) {
                while($row = mysqli_fetch_assoc($retval)) {
                    $folio = $row['folio'];
                    $query = "SELECT p.folio, p.fecha, u.nombre AS usuario FROM prestamo AS p INNER JOIN usuario AS u WHERE p.folio='$folio' AND p.id_usuario='$idUser' AND u.id=p.id_usuario;";
                    $consultData = $dbconex->query($query);
                    if(!$consultData){
                        echo mysqli_errors($dbconex);
                    }else{
                        if (mysqli_num_rows($consultData) > 0) {
                            while($row = mysqli_fetch_assoc($consultData)) {
                                $folio = $row['folio'];
                                $query = "SELECT COUNT(*) AS cantidad FROM detalle_libro WHERE folio=$folio";
                                $libros = $dbconex->query($query);
                                if(!$libros){
                                    echo mysqli_error($dbconex);
                                }else{
                                    if (mysqli_num_rows($libros) > 0) {
                                        $libro = mysqli_fetch_assoc($libros);
                                        $row['libros'] = $libro['cantidad'];
                                        $return_arr[] = $row;
                                    }
                                }
                            }
                        }
                    }
                }
                echo json_encode($return_arr);
            }
        }
    }

    function postDataUserTypeBibliotecario($dbconex){//vista index: mostrar prestamos a usuario bibliotecario
        $datos = [];
        $queryFolios = "SELECT DISTINCT folio FROM detalle_libro";
        $retval = $dbconex->query($queryFolios);
        if (mysqli_num_rows($retval) > 0) {
            while($row = mysqli_fetch_assoc($retval)) {
                $folio = $row['folio'];
                $query = "SELECT p.folio, p.fecha, u.nombre AS usuario FROM prestamo AS p INNER JOIN usuario AS u WHERE p.folio='$folio' AND u.id=p.id_usuario;";
                $consult = $dbconex->query($query);
                if(!$consult){
                    echo mysqli_error($dbconex);
                }else{
                    if (mysqli_num_rows($consult) > 0) {
                        while($row = mysqli_fetch_assoc($consult)) {
                           $folio = $row['folio'];
                           $query = "SELECT COUNT(*) AS cantidad FROM detalle_libro WHERE folio=$folio";
                           $libros = $dbconex->query($query);
                           if(!$libros){
                                echo mysqli_error($dbconex);
                            }else{
                                if (mysqli_num_rows($libros) > 0) {
                                    $libro = mysqli_fetch_assoc($libros);
                                    $row['libros'] = $libro['cantidad'];
                                    $datos[] = $row;
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($datos);
        }
    }

    function postUserSelect($dbconex){//enviar usuarios para create prestamo
        $return_arr = array();
        $estudiante = "Estudiante";
        $docente = "Docente";
        $activo = '1';
        $consult = "SELECT id, nombre FROM usuario WHERE tipo = '$estudiante' AND estatus = '$activo' OR tipo = '$docente' AND estatus = '$activo'";
        $tabla = $dbconex->query($consult);
        while($row = mysqli_fetch_array($tabla)){
            $return_arr[] = $row;
        }
        echo json_encode($return_arr);
        $dbconex->close();
    }

    function postFolio($dbconex){//obtener el folio siguiente a crear "id autoincrementable"
        $db = "Biblioteca";
        $table = "prestamo";
        $consult = "SELECT AUTO_INCREMENT AS folio FROM information_schema.tables WHERE TABLE_SCHEMA='$db' AND TABLE_NAME='$table'";
        $query = mysqli_query($dbconex,$consult) or die(mysqli_error($dbconex));
        if($row = mysqli_fetch_row($query)){
            $id = trim($row[0]);
            echo $id;
        }
    }

    function postBorrowedBooks($dbconex, $idUserBorrowedBooks){//obtener cantidad de libros prestados por usuario
        $consult = "SELECT libros_prestados FROM usuario WHERE id='$idUserBorrowedBooks'";
        $query = mysqli_query($dbconex,$consult) or die(mysqli_error($dbconex));
        if($row = mysqli_fetch_row($query)){
            $totalBorrowedBooks = trim($row[0]);
            echo $totalBorrowedBooks;
        }
        $dbconex->close();
    }

    function postBooksRepeater($dbconex){//obtener los libros para crear select repeater
        $output = '';
        $consult = "SELECT * FROM libros"; 
        $result = $dbconex->query($consult);
        while($row = $result->fetch_assoc()){
            foreach($result as $row)
            {
                $output .= '<option existencia="'.$row["existencia"].'" value="'.$row["id"].'">'.$row["titulo"].'</option>';
            }
            echo $output;  
        }
        $dbconex->close();
    }

    function createLoanBook($dbconex){//crear registro de material bibliografico
        if(isset($_POST['folio'])){
            $folio = $_POST['folio'];
        }
        if(isset($_POST['book'])){
            $book = $_POST['book'];
        }
        if(isset($_POST['extension'])){
            $extension = $_POST['extension'];
        }
        if(isset($_POST['date'])){
            $date = $_POST['date'];
        }
        if(isset($_POST['idRequestingUser'])){
            $idRequestingUser = $_POST['idRequestingUser'];
        }
        if(isset($_POST['idUserLoan'])){
            $idUserLoan = $_POST['idUserLoan'];
        }
        $totalLoan = count($book);
        $queryDetails = '';
        $queryBook = '';
        //guardando informacion en la tabla prestamo
        $resultLoan = $dbconex->query("INSERT INTO prestamo (fecha,id_usuario,id_usuario_prestamo)VALUES('$date','$idRequestingUser','$idUserLoan')");
        if(!$resultLoan){
            echo mysqli_error($dbconex);
        }else{
            //guardando informacion en la tabla datelle_libro
            for($i = 0; $i < count($book); $i++){
                $book_clean = mysqli_real_escape_string($dbconex, $book[$i]);
                $extension_clean = mysqli_real_escape_string($dbconex, $extension[$i]);
                if($book_clean != '' && $extension_clean != ''){
                    $queryDetails = $dbconex->query("INSERT INTO detalle_libro (folio, id_libro, extension, fecha_devolucion) VALUES ('$folio', '$book_clean', '$extension_clean', NULL)");
                }
            }        
            if(!$queryDetails){
                echo mysqli_error($dbconex);
            }else{
                for($j = 0; $j < count($book); $j++){//cambiando exitencia de libros por id
                    $book_clean = mysqli_real_escape_string($dbconex, $book[$j]);
                    if( $book_clean != ''){
                        $queryBook = $dbconex->query("UPDATE libros SET existencia = existencia - 1 WHERE id = '$book_clean'");
                    }
                }
                if(!$queryBook){
                    echo mysqli_error($dbconex);
                }else{
                    //actualizando valor de la columna libros prestados por id usuario
                    $querySumLoanUser = $dbconex->query("UPDATE usuario SET libros_prestados = libros_prestados + '$totalLoan' WHERE id = '$idRequestingUser'");
                    if(!$querySumLoanUser){
                        echo mysqli_error($dbconex);
                    }else{
                         //validando queries insertados correctamente
                        if($queryDetails != '' && $queryBook != '' && $resultLoan && $querySumLoanUser)
                            echo "success";
                        else
                            echo "error";
                        $dbconex->close();
                    }
                }
            }  
        }
    }

    function postDataLoanBook($dbconex){//visualizar detalles del prestamo
        if(isset($_POST['folio'])){
            $folio = $_POST['folio'];
            $return_arr = [];
            $penality = null;
            $query = "SELECT p.folio, e.nombre AS empleado, u.nombre AS usuario, p.fecha FROM prestamo AS p INNER JOIN usuario AS u INNER JOIN usuario AS e WHERE folio='$folio' and u.id=p.id_usuario and e.id=p.id_usuario_prestamo;";
            $dataPrestamo = $dbconex->query($query);
            if(!$dataPrestamo){
                echo mysqli_error($dbconex);
            }else{
                if(mysqli_num_rows($dataPrestamo) > 0){
                    $return_arr = mysqli_fetch_assoc($dataPrestamo);
                    $libros = [];
                    $query = "SELECT * FROM detalle_libro AS d INNER JOIN libros AS l WHERE folio=$folio and d.id_libro=l.id;";
                    $dataLibros = $dbconex->query($query);
                    if(!$dataLibros){
                        echo mysqli_error($dbconex);
                    }else{
                        $query = "SELECT * FROM penalizaciones WHERE folio_prestamo = $folio;";
                        $dataPenality = $dbconex->query($query);
                        if(!$dataPenality) {
                            echo mysqli_error($dbconex);
                        }else{
                            if (mysqli_num_rows($dataLibros) > 0) {
                                while($row = mysqli_fetch_assoc($dataLibros)){
                                    $libros[] = $row;
                                }
                                if (mysqli_num_rows($dataPenality) > 0) {
                                    $penality = mysqli_fetch_assoc($dataPenality);
                                }
                                $return_arr['libros'] = $libros;
                                $return_arr['penalizacion'] = $penality;
                                echo json_encode(array('data' => $return_arr));
                            }
                        }
                    }   
                }
            }
        }
    }

    function returnLoanBook($dbconex) {
        $return_arr = [];
        $penality = null;
        if (isset($_POST['folio'])) {
            $folio = $_POST['folio'];
        }
    
        $sql_prestamo = "SELECT p.folio, p.fecha, e.nombre AS empleado, u.nombre, u.id AS usuario ";
        $sql_prestamo .= "FROM prestamo AS p INNER JOIN usuario AS u INNER JOIN usuario AS e "; 
        $sql_prestamo .= "WHERE folio=$folio and u.id=p.id_usuario and e.id=p.id_usuario_prestamo;";
        $dataPrestamo = $dbconex->query($sql_prestamo);
        if(!$dataPrestamo){
            echo mysqli_error($dbconex);
        }else{
            if(mysqli_num_rows($dataPrestamo) > 0){
                $return_arr = mysqli_fetch_assoc($dataPrestamo);
                $books = [];
                $query = "SELECT l.titulo, l.isbn, d.extension, d.fecha_devolucion, l.id ";
                $query .= "FROM detalle_libro AS d INNER JOIN libros AS l ";
                $query .= "WHERE d.folio='$folio' AND d.id_libro=l.id;";
                $dataLibros = $dbconex->query($query);
                if(!$dataLibros){
                    echo mysqli_error($dbconex);
                }
                $query = "SELECT * FROM penalizaciones WHERE folio_prestamo = $folio;";
                $dataPenality = $dbconex->query($query);
                if(!$dataPenality) {
                    echo mysqli_error($dbconex);
                }else{
                    if (mysqli_num_rows($dataLibros) > 0) {
                        while($row = mysqli_fetch_assoc($dataLibros)){
                            $books[] = $row;
                        }
                        if (mysqli_num_rows($dataPenality) > 0) {
                            $penality = mysqli_fetch_assoc($dataPenality);
                        }
                        $return_arr['libros'] = $books;
                        $return_arr['penalizacion'] = $penality;
                        echo json_encode(array('data' => $return_arr));
                    }
                }   
            }
        }
    }

    function updateBorrowedBooks($dbconex){//Hacer devolucion de los libros
        if(isset($_POST['folio'])){
            $folio = $_POST['folio'];
        }
        if(isset($_POST['books'])){
            $books = json_decode($_POST['books']);
        }
        if(isset($_POST['idRequestingUser'])){
            $idRequestingUser = $_POST['idRequestingUser'];
        }
        if(isset($_POST['folioPenality'])){
            $folioPenality = $_POST['folioPenality'];
        }
        if(isset($_POST['reasonPenality'])){
            $reasonPenality = $_POST['reasonPenality'];
        }
        if(isset($_POST['totalPenality'])){
            $totalPenality = $_POST['totalPenality'];
        }
        if(isset($_POST['newRegistry'])){
            $newRegistry = $_POST['newRegistry'];
            registryNewPenality($dbconex, $folioPenality, $reasonPenality, $totalPenality, $newRegistry, $folio);
        }
        $totalLoan = count($books);
        $queryDetails = '';
        $queryBook = '';
        //guardando informacion en la tabla datelle_libro
        for($i = 0; $i < $totalLoan; $i++){
            $id_book = $books[$i]->id;
            $return_date = $books[$i]->returned_date;
            $queryDetails = $dbconex->query("UPDATE detalle_libro SET id_libro = '$id_book', fecha_devolucion = '$return_date' WHERE id_libro = $id_book AND folio = $folio");
            if(!$queryDetails){
                echo mysqli_error($dbconex);
                break;
            }
        }  
        for($i = 0; $i < $totalLoan; $i++){//cambiando exitencia de libros por id
            $id_book = $books[$i]->id;
            $queryBook = $dbconex->query("UPDATE libros SET existencia = existencia + 1 WHERE id = '$id_book'");
            if(!$queryBook){
                echo mysqli_error($dbconex);
                break;
            }
        }
        //actualizando valor de la columna libros prestados por id usuario
        $querySumLoanUser = $dbconex->query("UPDATE usuario SET libros_prestados = libros_prestados - '$totalLoan' WHERE id = '$idRequestingUser'");
        if(!$querySumLoanUser){
            echo mysqli_error($dbconex);
        }
        else{
            //validando queries insertados correctamente
            if($queryDetails != '' && $queryBook != '' && $querySumLoanUser)
                echo "success";
            else
                echo "error";
            $dbconex->close();
        }  
    }

    function postGetFolioPenality($dbconex) {
        $db = "Biblioteca";
        $table = "penalizaciones";
        $consult = "SELECT AUTO_INCREMENT AS folio FROM information_schema.tables WHERE TABLE_SCHEMA='$db' AND TABLE_NAME='$table'";
        $query = mysqli_query($dbconex,$consult) or die(mysqli_error($dbconex));
        if($row = mysqli_fetch_row($query)){
            $id = trim($row[0]);
            echo $id;
        }
    }

    function  registryNewPenality($dbconex, $folioPenality, $reasonPenality, $totalPenality, $newRegistry, $folio){
        $date = date("Y-m-d");
        if($newRegistry == "insert"){
            $query = "INSERT INTO penalizaciones (fecha, total, motivo, folio_prestamo) VALUES('$date', $totalPenality, '$reasonPenality', '$folio');";
        }else if($newRegistry == "update"){
            $query = "UPDATE penalizaciones set fecha = '$date', total = $totalPenality, motivo = '$reasonPenality' WHERE folio = $folioPenality";
        }
        $consult = $dbconex->query($query);
        if(!$consult){
            echo mysqli_error($dbconex);
        }
    }
?>
