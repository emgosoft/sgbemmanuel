<?php
require('../../fpdf/fpdf.php');
include('../../php/conexionDB.php');

class PDF extends FPDF
{
    // Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('../../img/logo.jpg',10,8,20);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título
        $this->Cell(30,10,utf8_decode('Universidad Politécnica'),0,1,'C');
        $this->Cell(200,10,utf8_decode('de la zona metropolitana de Guadalajara'),0,1,'C');
        // Salto de línea
        $this->Ln(25);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'C');
    }
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf->Cell(200,10,'Reporte libros de la  biblioteca', 0, 1, 'C');

//Cabecera de tabla
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Titulo',1,0,'C');
$pdf->Cell(24,8,'Autor',1,0,'C');
$pdf->Cell(36,8,'Editorial',1,0,'C');
$pdf->Cell(20,8,'ISBN',1,0,'C');
$pdf->Cell(40,8,utf8_decode('Año edición'),1,0,'C');
$pdf->Cell(40,8,'Existencia',1,1,'C');

$pdf->SetFont('Arial','',9);

//Busqueda en base de datos
$query = "SELECT * FROM libros;";
$retval = mysqli_query($dbconex, $query);
$filas = mysqli_num_rows($retval);
if ($filas > 0) {
    while($row = mysqli_fetch_assoc($retval)) { //Añadir fila a la tabla
        $pdf->Cell(35,8,utf8_decode($row['titulo']),1,0,'C');
        $pdf->Cell(24,8,$row['autor'],1,0,'C');
        $pdf->Cell(36,8,$row['editorial'],1,0,'C');
        $pdf->Cell(20,8,$row['isbn'],1,0,'C');
        $pdf->Cell(40,8,$row['anio_edicion'],1,0,'C');
        $pdf->Cell(40,8,$row['existencia'],1,1,'C');
    }
}

$pdf->Cell(100, 8, 'Total de libros: ' . $filas, 0, 1);

$pdf->Output();

//Cerrar conexion
mysqli_close($dbconex);
?>