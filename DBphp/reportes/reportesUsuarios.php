<?php
require('../../fpdf/fpdf.php');
include('../../php/conexionDB.php');

class PDF extends FPDF
{
    // Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('../../img/logo.jpg',10,8,20);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título
        $this->Cell(30,10,utf8_decode('Universidad Politécnica'),0,1,'C');
        $this->Cell(200,10,utf8_decode('de la zona metropolitana de Guadalajara'),0,1,'C');
        // Salto de línea
        $this->Ln(25);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'C');
    }
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf->Cell(200,10,'Reporte usuarios de la  biblioteca', 0, 1, 'C');

//Cabecera de tabla
$pdf->SetFont('Arial','B',9);
$pdf->Cell(35,8,'Nombre',1,0,'C');
$pdf->Cell(24,8,'# Celular',1,0,'C');
$pdf->Cell(36,8,'E-mail',1,0,'C');
$pdf->Cell(20,8,'Estatus',1,0,'C');
$pdf->Cell(40,8,'Tipo',1,0,'C');
$pdf->Cell(40,8,'Area',1,1,'C');

$pdf->SetFont('Arial','',9);

//Busqueda en base de datos
$query = "SELECT * FROM usuario;";
$retval = mysqli_query($dbconex, $query);
$filas = mysqli_num_rows($retval);
if ($filas > 0) {
    while($row = mysqli_fetch_assoc($retval)) { //Añadir fila a la tabla
        $estatus = ($row['estatus'] == '1')? 'Activo' : 'Inactivo';
        $pdf->Cell(35,8,$row['nombre'],1,0,'C');
        $pdf->Cell(24,8,$row['numero_celular'],1,0,'C');
        $pdf->Cell(36,8,$row['correo'],1,0,'C');
        $pdf->Cell(20,8,$estatus,1,0,'C');
        $pdf->Cell(40,8,$row['tipo'],1,0,'C');
        $pdf->Cell(40,8,$row['area'],1,1,'C');
    }
}

$pdf->Cell(100, 8, 'Total de usuarios: ' . $filas, 0, 1);

$pdf->Output();

//Cerrar conexion
mysqli_close($dbconex);
?>