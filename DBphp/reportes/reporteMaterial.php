<?php
require('../../fpdf/fpdf.php');
include('../../php/conexionDB.php');

class PDF extends FPDF
{
    // Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('../../img/logo.jpg',10,8,20);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título
        $this->Cell(30,10,utf8_decode('Universidad Politécnica'),0,1,'C');
        $this->Cell(200,10,utf8_decode('de la zona metropolitana de Guadalajara'),0,1,'C');
        // Salto de línea
        $this->Ln(25);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'C');
    }
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf->Cell(200,10,'Reporte material de la  biblioteca', 0, 1, 'C');

//Cabecera de tabla
$pdf->SetFont('Arial','B',12);
$pdf->Cell(35,8,'Tipo',1,0,'C');
$pdf->Cell(30,8,'Marca',1,0,'C');
$pdf->Cell(36,8,'Modelo',1,0,'C');
$pdf->Cell(50,8,'Estado',1,1,'C');

$pdf->SetFont('Arial','',12);

//Busqueda en base de datos
$query = "SELECT * FROM material;";
$retval = mysqli_query($dbconex, $query);
$filas = mysqli_num_rows($retval);
if ($filas > 0) {
    while($row = mysqli_fetch_assoc($retval)) { //Añadir fila a la tabla
        $pdf->Cell(35,8,utf8_decode($row['tipo']),1,0,'C');
        $pdf->Cell(30,8,$row['marca'],1,0,'C');
        $pdf->Cell(36,8,$row['modelo'],1,0,'C');
        $pdf->Cell(50,8,$row['estado'],1,1,'C');
    }
}

$pdf->Cell(100, 8, 'Total de material: ' . $filas, 0, 1);

$pdf->Output();

//Cerrar conexion
mysqli_close($dbconex);
?>