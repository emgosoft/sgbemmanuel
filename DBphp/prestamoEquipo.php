<?php
    include('../php/conexionDB.php');
    
    if(isset($_POST['action'])){//obtener vista de ejecucion
        $action = $_POST['action'];
        principal($dbconex, $action);
    }

    function principal($dbconex, $action){//funcion principal
        if(isset($_POST['idUser'])){
            $idUser = $_POST['idUser'];
        }
        if(isset($_POST['typeUser'])){
            $typeUser = $_POST['typeUser'];
        }
        if(isset($_POST['idUserBorrowedBooks'])){
            $idUserBorrowedBooks = $_POST['idUserBorrowedBooks'];
        }
        switch($action){
            case "index":
                postDataViewIndex($typeUser, $idUser, $dbconex);
                break;
            case "create":
                createLoanMaterial($dbconex);
                break;
            case "fillFolio":
                postFolio($dbconex);
                break;
            case "fillSelect":
                postUserSelect($dbconex);
                break;
            case "borrowedBooks":
                //postBorrowedBooks($dbconex, $idUserBorrowedBooks);
                break; 
            case "fillMaterialRepeater":
                postMaterialRepeater($dbconex);
                break;
            case "view":
                postDataLoanEquipment($dbconex);
                break;
            case "loanMaterials":
                returnLoanMaterial($dbconex);
                break;
            case "update":
                updateBorrowedMaterial($dbconex);
                break;
            case "receiveFolioPenality":
                postGetFolioPenality($dbconex);
                break;
            default: echo "error"; break;
        }
    }

    function postDataViewIndex($typeUser, $idUser, $dbconex){//enviar datos a la vista index
        if($typeUser != "Bibliotecario"){
            postDataUserTypeNoBibliotecario($typeUser, $idUser, $dbconex);
        }else{//todos los registros
            postDataUserTypeBibliotecario($dbconex);
        }
        $dbconex->close();
    }

    function postDataUserTypeNoBibliotecario($typeUser, $idUser, $dbconex){//vista index: mostrar prestamos por Usuario
        $return_arr = [];
        $queryFolios = "SELECT DISTINCT folio FROM detalle_material";
        $retval = $dbconex->query($queryFolios);
        if(!$retval){
            echo mysqli_error($dbconex);
        }else{
            if (mysqli_num_rows($retval) > 0) {
                while($row = mysqli_fetch_assoc($retval)) {
                    $folio = $row['folio'];
                    $query = "SELECT p.folio, p.fecha, u.nombre AS usuario FROM prestamo AS p INNER JOIN usuario AS u WHERE p.folio='$folio' AND p.id_usuario='$idUser' AND u.id=p.id_usuario;";
                    $consultData = $dbconex->query($query);
                    if(!$consultData){
                        echo mysqli_errors($dbconex);
                    }else{
                        if (mysqli_num_rows($consultData) > 0) {
                            while($row = mysqli_fetch_assoc($consultData)) {
                                $folio = $row['folio'];
                                $query = "SELECT COUNT(*) AS cantidad FROM detalle_material WHERE folio=$folio";
                                $materiales = $dbconex->query($query);
                                if(!$materiales){
                                    echo mysqli_error($dbconex);
                                }else{
                                    if (mysqli_num_rows($materiales) > 0) {
                                        $material = mysqli_fetch_assoc($materiales);
                                        $row['material'] = $material['cantidad'];
                                        $return_arr[] = $row;
                                    }
                                }
                            }
                        }
                    }
                }
                echo json_encode($return_arr);
            }
        }
    }

    function postDataUserTypeBibliotecario($dbconex){//vista index: mostrar prestamos a usuario bibliotecario
        $datos = [];
        $queryFolios = "SELECT DISTINCT folio FROM detalle_material";
        $retval = $dbconex->query($queryFolios);
        if (mysqli_num_rows($retval) > 0) {
            while($row = mysqli_fetch_assoc($retval)) {
                $folio = $row['folio'];
                $query = "SELECT p.folio, p.fecha, u.nombre AS usuario FROM prestamo AS p INNER JOIN usuario AS u WHERE p.folio='$folio' AND u.id=p.id_usuario;";
                $consult = $dbconex->query($query);
                if(!$consult){
                    echo mysqli_error($dbconex);
                }else{
                    if (mysqli_num_rows($consult) > 0) {
                        while($row = mysqli_fetch_assoc($consult)) {
                           $folio = $row['folio'];
                           $query = "SELECT COUNT(*) AS cantidad FROM detalle_material WHERE folio=$folio";
                           $materiales = $dbconex->query($query);
                           if(!$materiales){
                                echo mysqli_error($dbconex);
                            }else{
                                if (mysqli_num_rows($materiales) > 0) {
                                    $material = mysqli_fetch_assoc($materiales);
                                    $row['material'] = $material['cantidad'];
                                    $datos[] = $row;
                                }
                            }
                        }
                    }
                }
            }
            echo json_encode($datos);
        }
    }
    function postUserSelect($dbconex){//enviar datos a select2 de la vista create
        $return_arr = array();
        $docente = "Docente";
        $activo = "1";
        $consult = "SELECT * FROM usuario WHERE estatus = '$activo' AND tipo = '$docente'";
        $tabla = $dbconex->query($consult);
        while($row = mysqli_fetch_array($tabla)){
            $id = $row['id'];
            $nombre = $row['nombre'];
            $return_arr[] = array("id"=>$id, "nombre"=>$nombre);
        }
        echo json_encode($return_arr);
        $dbconex->close();
    }

    function postFolio($dbconex){//obtener el folio siguiente a crear "id autoincrementable"
        $db = "Biblioteca";
        $table = "prestamo";
        $consult = "SELECT AUTO_INCREMENT AS folio FROM information_schema.tables WHERE TABLE_SCHEMA='$db' AND TABLE_NAME='$table'";
        $query = mysqli_query($dbconex,$consult) or die(mysqli_error($dbconex));
        if($row = mysqli_fetch_row($query)){
            $id = trim($row[0]);
            echo $id;
        }
    }

    function postBorrowedBooks($dbconex, $idUserBorrowedBooks){//obtener cantidad de libros prestados por usuario
        $consult = "SELECT libros_prestados FROM usuario WHERE id='$idUserBorrowedBooks'";
        $query = mysqli_query($dbconex,$consult) or die(mysqli_error($dbconex));
        if($row = mysqli_fetch_row($query)){
            $totalBorrowedBooks = trim($row[0]);
            echo $totalBorrowedBooks;
        }
        $dbconex->close();
    }

    function postMaterialRepeater($dbconex){//obtener los libros para crear select repeater
        $output = '';
        $consult = "SELECT * FROM material"; 
        $result = $dbconex->query($consult);
        while($row = $result->fetch_assoc()){
            foreach($result as $row)
            {
                $output .= '<option marca="'.$row["marca"].'" tipo="'.$row["tipo"].'" estado="'.$row["estado"].'" value="'.$row["id"].'">'.$row["modelo"].'</option>';
            }
            echo $output;  
        }
        $dbconex->close();
    }

    function createLoanMaterial($dbconex){//crear registro de material bibliografico
        if(isset($_POST['folio'])){
            $folio = $_POST['folio'];
        }
        if(isset($_POST['material'])){
            $material = $_POST['material'];
        }
        if(isset($_POST['date'])){
            $date = $_POST['date'];
        }
        if(isset($_POST['idRequestingUser'])){
            $idRequestingUser = $_POST['idRequestingUser'];
        }
        if(isset($_POST['idUserLoan'])){
            $idUserLoan = $_POST['idUserLoan'];
        }
        $totalLoan = count($folio);
        $queryDetails = '';
        //guardando informacion en la tabla prestamo
        $resultLoan = $dbconex->query("INSERT INTO prestamo (fecha,id_usuario,id_usuario_prestamo)VALUES('$date','$idRequestingUser','$idUserLoan')");
        if(!$resultLoan){
            echo mysqli_error($dbconex);
        }else{
            //guardando informacion en la tabla datelle_material
            for($i = 0; $i < count($material); $i++){
                $material_clean = mysqli_real_escape_string($dbconex, $material[$i]);
                if($material_clean != ''){
                    $queryDetails = $dbconex->query("INSERT INTO detalle_material (folio, id_material, fecha_devolucion) VALUES ('$folio', '$material_clean', NULL)");
                }
            }
            if(!$queryDetails){
                echo mysqli_error($dbconex);
            }else{
               //validando queries insertados correctamente
                if($queryDetails != '' && $resultLoan != '')
                    echo "success";
                else
                    echo "error";
                $dbconex->close();
            }

        }
    }

    function postDataLoanEquipment($dbconex){
        if(isset($_POST['folio'])){
            $folio = $_POST['folio'];
            $return_arr = [];
            $penality = null;
            $query = "SELECT p.folio, e.nombre AS empleado, u.nombre AS usuario, p.fecha FROM prestamo AS p INNER JOIN usuario AS u INNER JOIN usuario AS e WHERE folio='$folio' and u.id=p.id_usuario and e.id=p.id_usuario_prestamo;";
            $dataPrestamo = $dbconex->query($query);
            if(!$dataPrestamo){
                echo mysqli_error($dbconex);
            }else{
                if(mysqli_num_rows($dataPrestamo) > 0){
                    $return_arr = mysqli_fetch_assoc($dataPrestamo);
                    $materiales = [];
                    $query = "SELECT * FROM detalle_material AS d INNER JOIN material AS m WHERE d.folio='$folio' AND d.id_material=m.id;";
                    $dataMateriales = $dbconex->query($query);
                    if(!$dataMateriales){
                        echo mysqli_error($dbconex);
                    }else{
                        $query = "SELECT * FROM penalizaciones WHERE folio_prestamo = $folio;";
                        $dataPenality = $dbconex->query($query);
                        if(!$dataPenality) {
                            echo mysqli_error($dbconex);
                        }else{
                            if (mysqli_num_rows($dataMateriales) > 0) {
                                while($row = mysqli_fetch_assoc($dataMateriales)){
                                    $materiales[] = $row;
                                }
                                if (mysqli_num_rows($dataPenality) > 0) {
                                    $penality = mysqli_fetch_assoc($dataPenality);
                                }
                                $return_arr['materiales'] = $materiales;
                                $return_arr['penalizacion'] = $penality;
                                echo json_encode(array('data' => $return_arr));
                            }   
                        }
                    }   
                }
            }
        }
    }
    function returnLoanMaterial($dbconex) {
        $return_arr = [];
        $penality = null;
        if (isset($_POST['folio'])) {
            $folio = $_POST['folio'];
        }
    
        $sql_prestamo = "SELECT p.folio, p.fecha, e.nombre AS empleado, u.nombre, u.id AS usuario ";
        $sql_prestamo .= "FROM prestamo AS p INNER JOIN usuario AS u INNER JOIN usuario AS e "; 
        $sql_prestamo .= "WHERE folio=$folio and u.id=p.id_usuario and e.id=p.id_usuario_prestamo;";
        $dataPrestamo = $dbconex->query($sql_prestamo);
        if(!$dataPrestamo){
            echo mysqli_error($dbconex);
        }else{
            if(mysqli_num_rows($dataPrestamo) > 0){
                $return_arr = mysqli_fetch_assoc($dataPrestamo);
                $materials = [];
                $query = "SELECT m.modelo, m.tipo, m.marca, m.estado, d.fecha_devolucion, m.id ";
                $query .= "FROM detalle_material AS d INNER JOIN material AS m ";
                $query .= "WHERE d.folio='$folio' AND d.id_material=m.id;";
                $dataMaterial = $dbconex->query($query);
                if(!$dataMaterial){
                    echo mysqli_error($dbconex);
                }
                $query = "SELECT * FROM penalizaciones WHERE folio_prestamo = $folio;";
                $dataPenality = $dbconex->query($query);
                if(!$dataPenality) {
                    echo mysqli_error($dbconex);
                }else{
                    if (mysqli_num_rows($dataMaterial) > 0) {
                        while($row = mysqli_fetch_assoc($dataMaterial)){
                            $materials[] = $row;
                        }
                        if (mysqli_num_rows($dataPenality) > 0) {
                            $penality = mysqli_fetch_assoc($dataPenality);
                        }
                        $return_arr['materiales'] = $materials;
                        $return_arr['penalizacion'] = $penality;
                        echo json_encode(array('data' => $return_arr));
                    }
                }   
            }
        }
    }

    function updateBorrowedMaterial($dbconex){//Hacer devolucion de los libros
        if(isset($_POST['folio'])){
            $folio = $_POST['folio'];
        }
        if(isset($_POST['materials'])){
            $materials = json_decode($_POST['materials']);
        }
        if(isset($_POST['idRequestingUser'])){
            $idRequestingUser = $_POST['idRequestingUser'];
        }
        if(isset($_POST['folioPenality'])){
            $folioPenality = $_POST['folioPenality'];
        }
        if(isset($_POST['reasonPenality'])){
            $reasonPenality = $_POST['reasonPenality'];
        }
        if(isset($_POST['totalPenality'])){
            $totalPenality = $_POST['totalPenality'];
        }
        if(isset($_POST['newRegistry'])){
            $newRegistry = $_POST['newRegistry'];
            registryNewPenality($dbconex, $folioPenality, $reasonPenality, $totalPenality, $newRegistry, $folio);
        }
        $totalLoan = count($materials);
        $queryDetails = '';
        //guardando informacion en la tabla datelle_material
        for($i = 0; $i < $totalLoan; $i++){
            $id_material = $materials[$i]->id;
            $return_date = $materials[$i]->returned_date;
            $queryDetails = $dbconex->query("UPDATE detalle_material SET id_material = '$id_material', fecha_devolucion = '$return_date' WHERE id_material = $id_material AND folio = $folio");
            if(!$queryDetails){
                echo mysqli_error($dbconex);
                break;
            }
        }  
        //validando queries insertados correctamente
        if($queryDetails != '')
            echo "success";
        else
            echo "error";
        $dbconex->close();
    }

    function postGetFolioPenality($dbconex) {
        $db = "Biblioteca";
        $table = "penalizaciones";
        $consult = "SELECT AUTO_INCREMENT AS folio FROM information_schema.tables WHERE TABLE_SCHEMA='$db' AND TABLE_NAME='$table'";
        $query = mysqli_query($dbconex,$consult) or die(mysqli_error($dbconex));
        if($row = mysqli_fetch_row($query)){
            $id = trim($row[0]);
            echo $id;
        }
    }

    function  registryNewPenality($dbconex, $folioPenality, $reasonPenality, $totalPenality, $newRegistry, $folio){
        $date = date("Y-m-d");
        if($newRegistry == "insert"){
            $query = "INSERT INTO penalizaciones (fecha, total, motivo, folio_prestamo) VALUES('$date', $totalPenality, '$reasonPenality', '$folio');";
        }else if($newRegistry == "update"){
            $query = "UPDATE penalizaciones set fecha = '$date', total = $totalPenality, motivo = '$reasonPenality' WHERE folio = $folioPenality";
        }
        $consult = $dbconex->query($query);
        if(!$consult){
            echo mysqli_error($dbconex);
        }
    }
?>
