<?php
    if(isset($_POST['action'])){//obtener vista de ejecucion
        include('../php/conexionDB.php');
        $action = $_POST['action'];
        principal($dbconex, $action);
    }
    if(isset($_GET['idUser'])){
        include('../../php/conexionDB.php');
        $idUser = $_GET['idUser'];
        list($idUser,
            $nombre,
            $numCelular,
            $password,
            $genero,
            $correo,
            $librosPrestados,
            $estatusText,
            $tipo,
            $area) = getDataUserUpdate($dbconex, $idUser);
    }

    function principal($dbconex, $action){//funcion principal
        switch($action){
            case "index":
                postDataViewIndex($dbconex);
                break;
            case "create":
                createUser($dbconex);
                break;
            case "view":
                postDataViewUser($dbconex);
                break;
            case "update":
                postUpdateUser($dbconex);
                break;
            default: echo "error"; break;
        }
    }
    function postDataViewIndex($dbconex){//mostrar informacion en dataTable
        $return_arr = array();
        $tabla = $dbconex->query("SELECT * FROM usuario");
        if(!$tabla){
            echo mysqli_error($dbconex);
        }else{
            while($row = mysqli_fetch_array($tabla)){
                $id = $row['id'];
                $nombre = $row['nombre'];
                $num_celular = $row['numero_celular'];
                $correo = $row['correo'];
                $area = $row['area'];
                $return_arr[] = array("id"=>$id,
                                    "nombre"=>$nombre,
                                    "numero_celular"=>$num_celular,
                                    "correo"=>$correo,
                                    "area"=>$area);
            }
            echo json_encode($return_arr);
        }
        $dbconex->close();
    }

    function postDataViewUser($dbconex){//
        $return_arr = array();
        if(isset($_POST['idUser'])){
            $idUser = $_POST['idUser'];
        }
        $consult = "SELECT * FROM usuario WHERE id = '$idUser'";
        $datos = $dbconex->query($consult);
        if(!$datos){
            echo mysqli_error($dbconex);
        }else{
            while($row = mysqli_fetch_array($datos)){
                $nombre = $row['nombre'];
                $numCelular = $row['numero_celular'];
                $password = $row['password'];
                $genero = $row['genero'];
                $correo = $row['correo'];
                $librosPrestados = $row['libros_prestados'];
                $estatus = $row['estatus'];
                $tipo = $row['tipo'];
                $area = $row['area'];
        
                $return_arr[] = array("nombre"=>$nombre,
                                    "numero_celular"=>$numCelular,
                                    "password"=>$password,
                                    "genero"=>$genero,
                                    "correo"=>$correo,
                                    "libros_prestados"=>$librosPrestados,
                                    "estatus"=>$estatus,
                                    "tipo"=>$tipo,
                                    "area"=>$area);
            }
            echo json_encode($return_arr);
        }
        $dbconex->close();
    }
    function createUser($dbconex){
        if (isset($_POST['nombre'])) {
            $nombre = $_POST['nombre'];
        }
        if (isset($_POST['numero_celular'])) {
            $numCelular = $_POST['numero_celular'];
        }
        if (isset($_POST['contrasenia'])) {
            $password = $_POST['contrasenia'];
        }
        if (isset($_POST['genero'])) {
            $genero = $_POST['genero'];
        }
        if (isset($_POST['correo'])) {
            $correo = $_POST['correo'];
        }
        if (isset($_POST['estatus'])) {
            $status = $_POST['estatus'];
        }
        if (isset($_POST['tipo'])) {
            $tipo = $_POST['tipo'];
        }
        if (isset($_POST['area'])) {
            $area = $_POST['area'];
        }
        $query = "INSERT INTO usuario (nombre,numero_celular,password,genero,correo,estatus,tipo,area) VALUES('$nombre','$numCelular','$password','$genero','$correo','$status','$tipo','$area') ";
        $queryConsult = mysqli_query($dbconex, $query);
        if(!$queryConsult){
            echo mysqli_errors($dbconex);
        }else{
            echo "success";
        }
        $dbconex->close();
    }
    function getDataUserUpdate($dbconex, $idUser){//obtener detalles usuario para editar
        $consult = "SELECT * FROM usuario WHERE id = '$idUser'";
        $datos = $dbconex->query($consult);
        if(!$datos){
            echo mysqli_errors($dbconex);
        }else{
            $fila = mysqli_fetch_array($datos);        
            $nombre = $fila['nombre'];
            $numCelular = $fila['numero_celular'];
            $password = $fila['password'];
            $genero = $fila['genero'];
            $correo = $fila['correo'];
            $librosPrestados = $fila['libros_prestados'];
            $estatus = $fila['estatus'];
            $tipo = $fila['tipo'];
            $area = $fila['area'];

            if($estatus == 1)
                $estatusText = "Activo";
            else if($estatus == 0)
                $estatusText = "Inactivo";

            if($genero == "M")
                $genero = "Masculino";
            else if($genero == "F")
                $genero = "Femenino";
            
            return array($idUser,$nombre,$numCelular,$password,$genero,$correo,$librosPrestados,$estatusText,$tipo,$area); 
        }
        $dbconex->close();
    }

    function postUpdateUser($dbconex){
        if(isset($_POST['id'])){
            $idUser = $_POST['id'];
        }
        if (isset($_POST['nombre'])) {
           $nombre = $_POST['nombre'];
       }
       if (isset($_POST['numero_celular'])) {
           $numCelular = $_POST['numero_celular'];
       }
       if (isset($_POST['contraseña'])) {
           $password = $_POST['contraseña'];
       }
       if (isset($_POST['genero'])) {
           $genero = $_POST['genero'];
       }
       if (isset($_POST['correo'])) {
           $correo = $_POST['correo'];
       }
       if(isset($_POST['libros_prestados'])){
           $librosPrestados = $_POST['libros_prestados'];
       }
       if (isset($_POST['estatus'])) {
           $status = $_POST['estatus'];
       }
       if (isset($_POST['tipo'])) {
           $tipo = $_POST['tipo'];
       }
       if (isset($_POST['area'])) {
           $area = $_POST['area'];
       }
       $sentencia = "UPDATE usuario SET nombre = '$nombre', numero_celular='$numCelular', password='$password', genero='$genero', correo='$correo', libros_prestados = '$librosPrestados',estatus = '$status', tipo = '$tipo', area = '$area' WHERE id = '".$idUser."'";
       $result =  $dbconex->query($sentencia); 
       if(!$result){
            echo mysqli_errors($dbconex);
            echo "error";
       }else{
           echo "success";
       }
       $dbconex->close();
    }
?>